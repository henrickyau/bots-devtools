module.exports = {
  type: 'react-component',
  npm: {
    esModules: true,
    umd: {
      global: 'BotsDevtools',
      externals: {
        react: 'React'
      }
    }
  },
  webpack: {
    extra: {
      module: {
        noParse: /node_modules\/json-schema\/lib\/validate\.js/
      },
      node: {
        console: true,
        fs: 'empty',
        net: 'empty',
        tls: 'empty'
      },
      resolve: {
        alias: {
          'react': require.resolve('react'),
          'react-dom': require.resolve('react-dom')
        }
      }
    }
  }
}
