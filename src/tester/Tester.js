import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Divider from 'material-ui/Divider';

import Conversation from '../conversation/Conversation.js'
import SendText from './SendText.js'

const Joi = require('joi-browser');
//const MessageModel = require('../common/MessageModel.js')(Joi);
//const MessageModel = require('../common/MessageModel.js')(Joi);
const {MessageModel} = require('@oracle/bots-node-sdk');

/*
 *  Tester component
 *  Synopsis:
 * 
 *  Props:
 *      Data:
 *         activeConversation
 *         selectedItemIndex
 *      Functions:
 *         sendMessage
 *         sendPostbackMessage
 *         selectItem
 * 
 *  State:
 *    testerHeight
 * 
 *  UI Event Handlers:
 * 
 *  Lifecycle:
 *    componentDidUpdate:
 *      adjust tester height
 *      scroll to end (if needed)
 * 
 *  Requirements on Parent:
 * 
 *  Notes:
 * 
 *  To Do:
 */
export default class Tester extends Component {
  constructor(props) {
    super(props);
    this.state = {
      testerHeight: this.props.targetHeight
    };
    this.sendTextMessage = this.sendTextMessage.bind(this);    
    this.selectAction = this.selectAction.bind(this);    
    this.setEndMessage = this.setEndMessage.bind(this);    
  }

  sendTextMessage(text) {
    let messagePayload = MessageModel.textConversationMessage(text);
    this.props.sendMessage(messagePayload);
  }

  setEndMessage(element) {
    this.conversationEndMessage = element;
  }

  selectAction(action, e) {
    console.debug(e);
    if (action.type === 'url') {
      this.openUrl(action);
    } else if (action.type === 'call') {
      this.callNumber(action);
    } else if (action.type === 'location') {
      this.requestLocation(action);
    } else if (action.type === 'postback'){
      this.sendPostback(action);
    } else {
      console.log('Not handling action', action);
    }
  }

  sendPostback(action) {
    if (action.type === 'postback') {
      let messagePayload = MessageModel.postbackConversationMessage(action.postback, action.label);
      this.props.sendMessage(messagePayload);
    } else {
      console.log('Not handling action', action);
    }
  }

  openUrl(action) {
    if (action.type === 'url' && action.url){
      var win = window.open(action.url, '_blank');
      win.focus();
    }
  }

  callNumber(action) {
    if (action.type === 'call' && action.phoneNumber){
      var win = window.open('tel:'+(action.phoneNumber.startsWith('+') ? action.phoneNumber : '+'+action.phoneNumber), '_blank');
      win.focus();  
    }  
  }

  requestLocation(locationAction) {
    var self = this;
    if (locationAction.type === 'location') {
      var staticLocation = {latitude: 37.2900055, longitude: -121.906558};
      if (window.location.protocol === 'https' || window.location.hostname === 'localhost') {
        if ("geolocation" in navigator) {
          /* geolocation is available */
          navigator.geolocation.getCurrentPosition(function(location) {
            console.debug(location.coords);
            self.sendLocation('Sending this location received from your browser', location.coords);
          });
        } else {
          /* geolocation IS NOT available */
          self.sendLocation('GeoLocation not supported.  Sending this pre-defined location instead.', staticLocation);
        }
      } else {
        self.sendLocation('GeoLocation is available only on https or localhost.  Sending this pre-defined location instead.', staticLocation);
      }
    }    
  }

  getMapURL(lat, lon) {
    return 'https://www.google.com/maps/search/?api=1&query='+lat+','+lon;
  }

  sendLocation(text, location) {
    let messagePayload = MessageModel.locationConversationMessage(
                                        location.latitude, 
                                        location.longitude, 
                                        text, 
                                        this.getMapURL(location.latitude, location.longitude));
    this.props.sendMessage(messagePayload);
  }

  componentDidUpdate(prevProps, prevState) {
    console.log('Tester componentDidUpdate');
    if (this.inputArea) {
      let testerHeight = this.props.targetHeight - this.inputArea.clientHeight;
      if (this.state.testerHeight !== testerHeight) {
        this.setState({testerHeight: testerHeight});
        console.log('setState testerHeight');
      }
    }

    if (prevProps.activeConversation && this.props.activeConversation) {
      if (prevProps.activeConversation.length !== this.props.activeConversation.length) {
        if (this.conversationEndMessage) {
          this.conversationEndMessage.scrollIntoView();
        }
      }
    }
  }

  render() {
    var conversationBoxStyle = {
      overflowY: 'auto',
      height: this.state.testerHeight + 'px',
    };
    return(
        <div>
            <div style={conversationBoxStyle} ref={(conversation) => { this.conversation = conversation; }}>
                <Conversation 
                  conversation={this.props.activeConversation} 
                  selectedItemIndex={this.props.selectedItemIndex}
                  selectAction={this.selectAction} 
                  setEndMessage={this.setEndMessage}
                  selectItem={this.props.selectItem}
                  />
            </div>               
            <div ref={(inputArea) => { this.inputArea = inputArea; }}>
                <Divider />
                <SendText sendTextMessage={this.sendTextMessage}/>
            </div>
        </div>
    );
  }

}

Tester.propTypes = {

  selectedItemIndex: PropTypes.number,
  targetHeight: PropTypes.number.isRequired,
  activeConversation: PropTypes.arrayOf(PropTypes.object),

  sendMessage: PropTypes.func,
  selectItem: PropTypes.func
}

