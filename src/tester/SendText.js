import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';

const rightStyle = {
  margin: 12,
  float: 'right'
};

const leftStyle = {
  overflow: 'hidden',
  marginLeft: '12px'
};

export default class SendText extends Component {
  constructor(props) {
    super(props);
    this.state = {
      messageText: null
    };
    this.sendMessage = this.sendMessage.bind(this);
    this.handleMessageText = this.handleMessageText.bind(this);
  }

  sendMessage(e) {
    console.log(e);
    console.log("sending", this.state.messageText);
    this.props.sendTextMessage(this.state.messageText);
  }

  handleMessageText(e) {
    this.setState({messageText: e.target.value});
  }

  render() {
    return (
      <div>
        <RaisedButton onClick={this.sendMessage} label="Send" primary={true} style={rightStyle} />
        <div style={leftStyle}>
          <TextField
            name='SendText_text'
            multiLine={true}
            fullWidth={true}
            onChange={this.handleMessageText}
          />
        </div>
        <div/>
      </div>
    );
  }
};

