import 'whatwg-fetch'; // mock fetch for jest(node), otherwise had issue with FetchResponse/FetchRequest headers not iterable
//import 'isomorphic-fetch';
//require('isomorphic-fetch');
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

import fetch from 'jest-fetch-mock'; // mock fetch for testing
global.fetch = fetch;

if (!HTMLElement.prototype.scrollIntoView) {
    HTMLElement.prototype.scrollIntoView = () => {}
}
