import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Tester from '../tester/Tester.js';

import ChatEnvelopeMessageModel from '../common/chatEnvelopeMessageModel.js';
import WsMessageModel from '../common/wsMessageModel.js';

const WebsocketConnection = require('./WebsocketConnection.js');

/*
 *  WebsocketTester component
 *  Synopsis:
 * 
 *  Props:
 *      Data:
 *        chatServerUrl
 *        channelId
 *        conversationProperties
 *        targetHeight
 *        userId
 *        initialConversation
 *      Functions:
 *        returnWsTesterState
 *        setTesterProperties
 *        returnError
 * 
 *  State:
 *     activeConversation
 *     selectedItemIndex
 *     lastSelfTime
 *     lastBotTime
 * 
 *  UI Event Handlers:
 * 
 *  Lifecycle:
 *    componentDidUpdate:
 *      check ws connectio difference???
 *    componnentWillUnmount:
 *      close ws connection
 * 
 *  Requirements on Parent:
 * 
 *  Notes:
 * 
 *  To Do:
 */

export default class WebsocketTester extends Component {
  constructor(props) {
    super(props);
    this.sendMessage = this.sendMessage.bind(this);    
    this.addSelfMessageToConversation = this.addSelfMessageToConversation.bind(this);    
    this.returnWsTesterState = this.returnWsTesterState.bind(this);    
    this.handleWsMessageEvent = this.handleWsMessageEvent.bind(this);    
    this.selectItem = this.selectItem.bind(this);    

    this.state = {
      activeConversation: (this.props.initialConversation ? this.props.initialConversation : []),
      selectedItemIndex: -1,
      lastSelfTime: 0,
      lastBotTime: 0
    };

    if (this.props.chatServerUrl && this.props.channelId) {
      this.websocketConnection = this.createWsConnection(
        this.props.chatServerUrl, 
        this.props.userId
      );
      this.websocketConnection.connectWs();
    }
  }

  get wsConnection() {
    return this.websocketConnection;
  }

  computeWebsocketUrl(chatServerUrl, userId) {
    let wsUrl = (chatServerUrl ? chatServerUrl.replace(/\/$/, '') : null);
    if (wsUrl) {
        if (wsUrl.endsWith("/chat")) {
            wsUrl = wsUrl + "/ws";
        } else if (wsUrl.endsWith("/chat/bots")) {
            wsUrl = wsUrl.substr(0, wsUrl.length-5)+ "/ws";
        }
    
        if (wsUrl.startsWith('https://')) {
            wsUrl = wsUrl.replace('https://','wss://');
        } else if (wsUrl.startsWith('http://')) {
            wsUrl = wsUrl.replace('http://','ws://');
        } 
        wsUrl = wsUrl + '?user=' + userId;
    }
    return wsUrl;
  }

  createWsConnection(chatServerUrl, userId){
    return new WebsocketConnection({
      wsUrl: this.computeWebsocketUrl(chatServerUrl, userId),
      returnState: this.returnWsTesterState,
      handleWsMessageEvent: this.handleWsMessageEvent
    });
  }

  addSelfMessageToConversation(selfMessage) {
    var timestamp = Date.now();
    let chatEnvelopeMessageInfo = {
      timestamp: timestamp,
      fromType: 'USER',
      responseTime: (this.state.lastBotTime ? timestamp - this.state.lastBotTime : 0)
    };
    let chatEnvelopeMessage = ChatEnvelopeMessageModel.chatEnvelopeMessage(selfMessage.messagePayload, selfMessage, chatEnvelopeMessageInfo);
    if (chatEnvelopeMessage){
      var activeConversation = this.state.activeConversation;
      activeConversation.push(chatEnvelopeMessage);
  
      this.setState({activeConversation: activeConversation, lastSelfTime: timestamp});
      if (this.props.setNavigation){
        this.props.setNavigation(null);
      }  
    } else {
      console.log('Invalid chatEnvelope message created from: ', selfMessage, chatEnvelopeMessageInfo);
    }
  }

  addBotMessageToConversation(botResponse) {
    var timestamp = Date.now();
    let chatEnvelopeMessageInfo = {
      timestamp: timestamp,
      fromType: 'BOT',
      responseTime: (this.state.lastSelfTime ? timestamp - this.state.lastSelfTime : 0)
    };
    let chatEnvelopeMessage = ChatEnvelopeMessageModel.chatEnvelopeMessage(botResponse.body.messagePayload, botResponse, chatEnvelopeMessageInfo);
    if (chatEnvelopeMessage) {
      var activeConversation = this.state.activeConversation.slice(0);
      activeConversation.push(chatEnvelopeMessage);
      if (botResponse.messagePayload && botResponse.messagePayload.channelExtensions) {
        if (botResponse.messagePayload.channelExtensions) {
          if (this.props.setNavigation){
            this.props.setNavigation(botResponse.messagePayload.channelExtensions);
          }
        }
      }
      this.setState({activeConversation: activeConversation, lastBotTime: timestamp});
    } else {
      console.log('Invalid chatEnvelope message created from: ', botResponse, chatEnvelopeMessageInfo);
    }
  }

  sendMessage(messagePayload) {
    let message = WsMessageModel.toBotMessage(messagePayload, this.props.channelId, this.props.conversationProperties);
    this.addSelfMessageToConversation(message);
    if (this.websocketConnection){
        this.websocketConnection.sendMessage(message);
    }
  }

  handleWsMessageEvent(event){
    var msg = JSON.parse(event.data);
    if (msg.from.type === 'bot') {
        //var botResponse = msg.body;
        this.addBotMessageToConversation(msg);
        this.props.returnError('');
    } else {
        var errorResponse;
        if (msg.error && msg.error.message) {
            errorResponse = msg.error.message;
        } else {
            errorResponse = JSON.stringify(msg);
        }
        this.props.returnError(errorResponse);
    }
  }

  getSummaryTesterProperties(){
    return {
      wsInfo: this.wsConnection.info()
    };
  }

  updateSummaryTesterProperties() {
    if (this.props.setTesterProperties){
      this.props.setTesterProperties(this.getSummaryTesterProperties());
    }
  }

  selectItem(itemIndex) {
    if (itemIndex !== -1){
      if (itemIndex !== this.state.selectedItemIndex) {
        this.setState({selectedItemIndex: itemIndex});
        this.props.setTesterProperties(this.state.activeConversation[itemIndex]);  
      } else {
        this.setState({selectedItemIndex: -1});
        this.updateSummaryTesterProperties();
      }
    } 
  }

  returnWsTesterState() {
    if (this.props.returnWsTesterState) {
        this.props.returnWsTesterState({
          wsUrl: this.wsConnection.wsUrl,
          wsStateName: this.wsConnection.wsReadyStateName,
        });
    }
    if (this.state.selectedItemIndex === -1) {
      this.updateSummaryTesterProperties();
    }
  }

  componentWillUnmount() {
    if (this.websocketConnection) {
      this.websocketConnection.close();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    var newWsConnection;
    var self = this;
    console.log('WebsocketTester componentDidUpdate');

    function updateWs(){
      if (self.websocketConnection) {
        self.websocketConnection.close();
        self.websocketConnection.stopUpdate();
      }
      self.websocketConnection = newWsConnection;
      if (self.websocketConnection) {
        self.websocketConnection.connectWs();
        console.log('WebsocketTester connectWs');
      } else {
        console.log('WebsocketTester returnComponentState');
        self.returnComponentState(this);
        //self.setWsStateUrl('UNOPENED', '');
      }
      self.props.returnError('');
    }

    //if (this.props.chatServerUrl !== prevProps.chatServerUrl || this.props.channelId !== prevProps.channelId) {
    if (this.props.chatServerUrl && this.props.channelId) {
      newWsConnection = this.createWsConnection(
        this.props.chatServerUrl, 
        this.props.userId
      );
    }

    if (newWsConnection && this.websocketConnection) {
      if (newWsConnection.wsUrl !== this.websocketConnection.wsUrl) {
        console.log('WS Connection updated from', this.websocketConnection.wsUrl, 'to', newWsConnection.wsUrl);
        updateWs();
      } else {
        if (this.websocketConnection.wsReadyStateName === 'CLOSED' || this.websocketConnection.wsReadyStateName === 'UNKNOWN') {
          console.log('WS not changed, closed or error connection,  No retries');
          //updateWs();
        } else {
          console.log('WS not changed');
        }
      }
    } else if (newWsConnection && !this.websocketConnection) {
      console.log('WS Connection updated to', newWsConnection.wsUrl);
      updateWs();
    } else if (!newWsConnection && this.websocketConnection) {
      console.log('WS Connection reset from', this.websocketConnection.wsUrl);
      updateWs();
    } else if (!newWsConnection && !this.websocketConnection){
      console.log('No WS connection old or new');
    }
  }

  render() {

    return(
        <div>
            <Tester 
                //conversationBoxStyle={this.props.conversationBoxStyle} 
                activeConversation={this.state.activeConversation} 
                sendMessage={this.sendMessage}
                targetHeight={this.props.targetHeight}
                selectItem={(this.props.hideItemProperties ? null : this.selectItem)}
                selectedItemIndex={this.state.selectedItemIndex}
                />
        </div>
    );
  }

}

WebsocketTester.propTypes = {

  chatServerUrl: PropTypes.string.isRequired,
  channelId: PropTypes.string.isRequired,
  conversationProperties: PropTypes.object.isRequired,
  targetHeight: PropTypes.number.isRequired,
  userId: PropTypes.string,
  initialConversation: PropTypes.arrayOf(PropTypes.object),

  returnWsTesterState: PropTypes.func,
  setTesterProperties: PropTypes.func,
  returnError: PropTypes.func
}
