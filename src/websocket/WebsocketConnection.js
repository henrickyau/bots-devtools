const WebsocketConnection = class {
    /**
     * @constructor
     * @param {object} config - The config object
     */
    constructor(config) {
        this._wsUrl = config.wsUrl;
        this.handleWsMessageEvent = config.handleWsMessageEvent;
        this.returnStateToParent = config.returnState;
        this.detached = false;
        this.openSince = null;
        this.messagesReceived = 0;
        this.messagesSent = 0;
    }

    get wsUrl() {
        if (this._ws){
            return this._ws.url;
        }
        else {
            return this._wsUrl;
        }
    }

    get ws() {
        return this._ws;
    }

    get wsReadyStateName() {
        if (this._ws) {
            var result = '';
            switch(this._ws.readyState) {
                case 0:
                    result = 'CONNECTING';
                    break;
                case 1:
                    result = 'OPEN';
                    break;
                case 2:
                    result = 'CLOSING';
                    break;
                case 3:
                    result = 'CLOSED';
                    break;
                default:
                    result = 'UNKNOWN';
                    break;
            }
            return result;
        } else {
            return 'UNOPENED';
        }
    }

    close() {
        if (this._ws) {
            this._ws.close();
        }
    }

    returnState(){
        if (!this.detached) {
            this.returnStateToParent(this);
        }
    }

    stopUpdate(){
        this.detached = true;
    }

    info() {
        return {
            state: this.wsReadyStateName,
            url: this.wsUrl,
            openSince: this.openSince,
            messagesReceived: this.messagesReceived,
            messagesSent: this.messagesSent
        };
    }

    sendMessage(message){
        if (this._ws && (this._ws.readyState === this._ws.OPEN)) {
            this._ws.send(JSON.stringify(message));
            this.messagesSent++;
            this.returnState();
            //this.addSelfMessage(textM);
        } else {
            console.log('Failed to send text message', message);
            console.log('Websocket not OPEN', this._ws);
        }       
    }

    connectWs(){
            var self = this;
            var ws = new WebSocket(this._wsUrl);
            this._ws = ws;
            ws.onopen = function(event) {
                console.log('WS open');
                self.openSince = Date.now();
                self.returnState();
            };
            ws.onclose = function(event) {
                console.log('WS close');
                console.log(self._wsUrl);
                self.returnState();
            };
            ws.onerror = function(event) {
                console.log('WS error');
                self.returnState();
            };
            ws.onmessage = function wsMessage(event) {
                console.log('WebSocket message: ' + event.data);
                self.messagesReceived++;
                if (self.handleWsMessageEvent) {
                    self.handleWsMessageEvent(event);
                }
                self.returnState();
            };
            self.returnState();
            console.log("Setting WS:", ws);
          }
}

module.exports = WebsocketConnection;
