import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Async from 'react-promise'

export default class PropertyListings extends Component {

    getObjectDisplay(obj) {
        return <pre style={{whiteSpace: "pre-wrap"}}><code>{JSON.stringify(obj, null, 2)}</code></pre>;
    }

    getValueDisplay(value) {
        const self = this;
        if (value == null) return <span/>;
            if (typeof value.then === 'function') {
                return <Async promise={value} then={val => <span>{(typeof val === 'object' ? self.getObjectDisplay(val) : val)}</span>} />;
            } else if (typeof value === 'object'){
                return <span>{this.getObjectDisplay(value)}</span>;
            } else if (typeof value === 'boolean'){
                return <span>{String(value)}</span>;
            } else {
                return <span>{value}</span>;
            }    
    }

    render() {

        let self = this;

        if (this.props.propertyValues) {

            const propertiesDisplay = this.props.propertyValues.map((propertyValue, index) =>
                <div key={index}>
                    {propertyValue.name && 
                    <div>
                        <br/>
                            <div>
                            <span style={{color: 'blue', fontStyle: 'italic', paddingLeft: '5px'}}>{propertyValue.name}:</span> 
                            <span> {self.getValueDisplay(propertyValue.value)} </span>
                            </div>               
                    </div>
                    }
                </div>);

            return (
                <div style={{overflowWrap: 'break-word'}}>
                    {propertiesDisplay}
                </div>
                );
        
        } else {
            return (
                <div style={{overflowWrap: 'break-word'}}>
                </div>
                );
        }
    }



}

PropertyListings.propTypes = {

    propertyValues: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string.isRequired,
        value: PropTypes.isRequired
    }))

}