
    function constructFetchHeaders(fetchReqResp){
        var headersObj = {};
        if (fetchReqResp.headers) {
            console.log("Fetch Headers", fetchReqResp.headers);
            if (fetchReqResp.headers instanceof Headers){
                //console.log("Fetch Headers Entries", fetchReqResp.headers.entries());
                for (var pair of fetchReqResp.headers) {
                    headersObj[pair[0]] = pair[1];
                }
            }
        }
        return headersObj;    
    }

    function constructFetchBody(fetchReqResp){
        if (fetchReqResp.bodyUsed) {
            console.err("fetchReqResp body already returned");
            return "";
        }
        return fetchReqResp.text()
            .then((text) => {
                try {
                    return JSON.parse(text);
                } catch (err) {
                    //console.log("constructFetchBody JSON parse", err);
                    return text;
                }
            })
            .catch((err) => {
                //console.log("fetchReqResp.text error", err);
                return "";
            });
    }

    function getFetchData(fetchReqResp, requestProperties) {
        var result = [];
        let self = this;
        requestProperties.forEach(function (reqProp) {
            let value;
            if (reqProp.resolver){
                value = reqProp.resolver(fetchReqResp);
            } else {
                value = fetchReqResp[reqProp.name];
            }
            if (value != null && value !== ""){
                result.push({
                    name: reqProp.name,
                    value: value
                });    
            }
        });
        return result;
    }


module.exports = {
    getFetchData: getFetchData,
    constructFetchBody: constructFetchBody,
    constructFetchHeaders: constructFetchHeaders
};
