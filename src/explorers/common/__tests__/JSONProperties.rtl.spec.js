import React from 'react';
import {render, Simulate, wait} from 'react-testing-library';
import 'dom-testing-library/extend-expect';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import JSONProperties from '../JSONProperties';

describe("JSONProperties", () => {
    it("should render default empty JSONProperties", () => {
        const mockCallback = jest.fn();
        const Rendered = render(
            <MuiThemeProvider>
                <JSONProperties returnProperties={mockCallback}/>
            </MuiThemeProvider>);

        expect(Rendered.getByLabelText('JSON Properties')).toHaveTextContent(JSON.stringify({}));
    });
});

describe("JSONProperties Props and State", () => {

    const mockCallback = jest.fn();
    const myProperties = {type: 'simple'};
    const myLabel = 'myLabel';
    let Rendered;

    beforeEach(() => {
        Rendered = render(
            <MuiThemeProvider>
                <JSONProperties 
                    returnProperties={mockCallback}
                    label={myLabel}
                    properties={myProperties}
                />
            </MuiThemeProvider>);
    });

    it("should reflect props in output", () => {
        expect(Rendered.getByLabelText(myLabel)).toHaveTextContent(JSON.stringify(myProperties, null, 4));
    });

    it("should reflect updated props in output", () => {
        const myPropertiesNew = {type: 'complex'};
        Rendered.rerender(
            <MuiThemeProvider>
            <JSONProperties 
                returnProperties={mockCallback}
                label={myLabel}
                properties={myPropertiesNew}
            />
            </MuiThemeProvider>);
        expect(Rendered.getByLabelText(myLabel)).toHaveTextContent(JSON.stringify(myPropertiesNew, null, 4));
    });

    it("should support direct state update", () => {
        expect(Rendered.getByLabelText(myLabel)).toHaveTextContent(JSON.stringify(myProperties, null, 4));
        const myPropertiesNew = {type: 'stateUpdated'};
        const textInputNode = Rendered.getByLabelText(myLabel);
        textInputNode.value = JSON.stringify(myPropertiesNew);
        Simulate.change(textInputNode);
        expect(Rendered.getByLabelText(myLabel)).toHaveTextContent(JSON.stringify(myPropertiesNew));
    });

    it("should report JSON error of invalid properties", () => {
        expect(Rendered.queryByText('Not valid JSON')).not.toBeInTheDOM();
        const stringInvalid = "{'hello': 'world}";
        let textInputNode = Rendered.getByLabelText(myLabel);
        textInputNode.value = stringInvalid;
        Simulate.change(textInputNode);
        expect(Rendered.queryByText('Not valid JSON')).toBeInTheDOM();
        const stringValid = "{\"hello\": \"world\"}";
        textInputNode = Rendered.getByLabelText(myLabel);
        textInputNode.value = stringValid;
        Simulate.change(textInputNode);
        expect(Rendered.queryByText('Not valid JSON')).not.toBeInTheDOM();
    });

});

