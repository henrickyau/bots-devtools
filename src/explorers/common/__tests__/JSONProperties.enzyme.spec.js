import React from 'react';
import { shallow } from 'enzyme';
import JSONProperties from '../JSONProperties';

describe("JSONProperties", () => {
  it("should render JSONProperties", () => {
    const mockCallback = jest.fn();
    const wrapper = shallow(<JSONProperties returnProperties={mockCallback}/>);
  });

  it("should match snapshot", () => {
    const mockCallback = jest.fn();
    const myProperties = {type: 'simple'};
    const myLabel = 'myLabel';
    const wrapper = shallow(<JSONProperties 
                              returnProperties={mockCallback}
                              label={myLabel}
                              properties={myProperties}
                              />);
    expect(wrapper.getElements()).toMatchSnapshot();
  });

  it("should reflect props in output", () => {
    const mockCallback = jest.fn();
    const myProperties = {type: 'simple'};
    const myLabel = 'myLabel';
    const wrapper = shallow(<JSONProperties 
                              returnProperties={mockCallback}
                              label={myLabel}
                              properties={myProperties}
                              />);
    expect(wrapper.find('TextField').prop('floatingLabelText')).toEqual(myLabel);
    expect(wrapper.find('TextField')).toHaveLength(1);
    expect(JSON.parse(wrapper.find('TextField').prop('value'))).toEqual(myProperties);
  });
 
  it("should reflect updated props in output", () => {
    const mockCallback = jest.fn();
    const properties = {type: 'simple'};
    const propertiesNew = {type: 'complex'};
    const wrapper = shallow(<JSONProperties 
                              returnProperties={mockCallback}
                              properties={properties}
                              />);
    expect(wrapper.find('TextField')).toHaveLength(1);
    expect(JSON.parse(wrapper.find('TextField').prop('value'))).toEqual(properties);
    expect(JSON.parse(wrapper.setProps({returnProperties: mockCallback,
                                        properties: propertiesNew})
                        .find('TextField').prop('value'))).toEqual(propertiesNew);
  });


  describe("JSONProperties Advanced", () => {

    const mockCallback = jest.fn();
    const properties = {type: 'simple'};
    const propertiesNew = {type: 'stateUpdated'};
    let wrapper;
    let instance;

    beforeEach(() => {
      wrapper = shallow(<JSONProperties 
        returnProperties={mockCallback}
        properties={properties}
        />);
        instance = wrapper.instance();
        /*instance.updatePropertiesStr(JSON.stringify(propertiesNew), function(){
          done();
        });*/
        //wrapper.find('TextField').simulate('change',JSON.stringify(propertiesNew));
    });

    it("should support direct state update", () => {
      wrapper.setState({propertiesStr: JSON.stringify(propertiesNew)});
      expect(JSON.parse(instance.state.propertiesStr)).toEqual(propertiesNew);
      expect(JSON.parse(wrapper.instance().state.propertiesStr)).toEqual(propertiesNew);
      expect(JSON.parse(wrapper.find('TextField').prop('value'))).toEqual(propertiesNew);

      //expect(JSON.parse(wrapper.find('TextField').simulate('change',JSON.stringify(propertiesNew)).prop('value'))).toEqual(properties);
      //expect(JSON.parse(wrapper.setState({propertiesStr: JSON.stringify(propertiesNew)})
      //                    .find('TextField').prop('value'))).toEqual(propertiesNew);
    });

  });

  describe("JSONProperties Advanced Error", () => {

    const mockCallback = jest.fn();
    const properties = {type: 'simple'};
    const propertiesNew = {type: 'stateUpdated'};
    const stringInvalid = "{'hello': 'world}";
    const stringValid = "{\"hello\": \"world\"}";
    let wrapper;
    let instance;

    beforeEach((done) => {
      wrapper = shallow(<JSONProperties 
        returnProperties={mockCallback}
        properties={properties}
        />);
        instance = wrapper.instance();
        instance.updatePropertiesStr(stringInvalid, function(){
          done();
        });
    });
  
    it("bad update results in error", () => {
      expect(wrapper.state().propertiesError).toEqual('Not valid JSON');
      //expect(wrapper.find('TextField').prop('errorText')).toEqual('Not valid JSON');
    });
  
  });

});
