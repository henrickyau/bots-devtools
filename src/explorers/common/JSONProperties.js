import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TextField from 'material-ui/TextField';

import _ from 'underscore';

/*
 *  JSONProperties component
 *  Synopsis:
 * 
 *  Props:
 *      Data:
 *        properties
 *        label
 *      Functions:
 *        returnProperties
 * 
 *  State:
 *      propertiesStr
 *      propertiesError
 * 
 *  Lifecycle:
 *      componentWillReceiveProps
 *          If props.properties changes (effected by parent), and that new props did not result from state update, then update the state
 * 
 *  Notes:
 * 
 *  To Do:
 */
export default class JSONProperties extends Component {
    constructor(props) {
        super(props);

        this.properties = this.props.properties || {};

        this.state = {
            propertiesStr: this.stringifyProperties(this.properties, null, 4),
            propertiesError: ''
        }

    }

    handleSetPropertiesStr = (event, value) => {
        this.updatePropertiesStr(value);
    };

    stringifyProperties(properties) {
        return JSON.stringify(properties, null, 4);
    }

    parsePropertiesStr(propertiesStr, callback) {
        let propertiesObj;
        try {
            propertiesObj = JSON.parse(propertiesStr);
            callback(null, propertiesObj);
        } catch(e) {
            callback(e);
        }
    }

    returnProperties(state, properties){
        /*this.setState(state);
        if (properties) { // do not return properties till its valid.  componentWillReceiveProps will check if props has been updated before changing state.
            this.props.returnProperties(properties);
        }*/
        this.setState(state, function(){
            if (properties) { // do not return properties till its valid.  componentWillReceiveProps will check if props has been updated before changing state.
                this.props.returnProperties(properties);
            }
        });

        /* this does not work as this will kick off componentDidUpdate before properties are returned to parent
        this.setState(state, function(){
            self.props.returnProperties(state);
        });*/
    }

    updatePropertiesStr(newValue, callback) {
        var self = this;
        newValue = (newValue ? newValue : '{}');
            this.parsePropertiesStr(newValue, function(err, propertiesObj){
                if (!err) {
                    self.returnProperties({
                      propertiesStr: newValue,
                      propertiesError: ''
                    }, propertiesObj);
                    if (callback) {
                        callback(null, propertiesObj);
                    }
                } else {
                    self.returnProperties({
                      propertiesStr: newValue,
                      propertiesError: 'Not valid JSON'
                    }, null);
                    if (callback) {
                        callback(err);
                    }
                }
            });
    }

    /*componentDidUpdate() {
        if (this.props.properties) {
            if (this.stringifyProperties(this.props.properties) !== this.state.propertiesStr) {
                this.updatePropertiesStr(this.stringifyProperties(this.props.properties));
            }    
        }
    }*/

    componentWillReceiveProps(nextProps) {
        if (!_.isEqual(this.props.properties, nextProps.properties)) {
            if (!_.isEqual(JSON.parse(this.state.propertiesStr), nextProps.properties)) {
                this.updatePropertiesStr(this.stringifyProperties(nextProps.properties));
            }
        }    
    }

    render() {

        return (
            <div>
                <TextField
                    multiLine={true}
                    rows={6}
                    rowsMax={this.props.maxRows}
                    fullWidth={true}
                    floatingLabelText={(this.props.label ? this.props.label : "JSON Properties")}
                    value={this.state.propertiesStr}
                    onChange={this.handleSetPropertiesStr}
                    errorText={this.state.propertiesError}
                    textareaStyle={{fontSize: '12px', lineHeight: 1.5}}
                />
                <br/>
            </div>
        );
    }
}

JSONProperties.propTypes = {

    properties: PropTypes.object,
    label: PropTypes.string,
    maxRows: PropTypes.number,
  
    returnProperties: PropTypes.func.isRequired
}