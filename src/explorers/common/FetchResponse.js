import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PropertyListings from './FetchRequestResponse/PropertyListings.js'

//import '../explorers.css';

import fetch_util from "./fetch_util";

export default class FetchResponse extends Component {
    constructor(props) {
        super(props);

        this.responseProperties = [
            {name: 'url', type: 'string'},
            {name: 'status', type: 'string'},
            {name: 'statusText', type: 'string'},
            {name: 'headers', type: 'string', resolver: fetch_util.constructFetchHeaders},
            {name: 'type', type: 'string'},
            {name: 'ok', type: 'string'},
            {name: 'redirected', type: 'string'},
            {name: 'useFinalURL', type: 'string'},
            {name: 'body', type: 'string', resolver: fetch_util.constructFetchBody},
        ];

        this.state = {
            fetchResponseData: fetch_util.getFetchData(this.props.fetchResponse, this.responseProperties)
        };

    }

    componentWillReceiveProps(nextProps) {
        if (this.props.fetchResponse !== nextProps.fetchResponse) {
            this.setState({fetchResponseData: fetch_util.getFetchData(nextProps.fetchResponse, this.responseProperties)});
        }
    }

    render() {

        return (
            <div>
                        <h4>Response</h4>
                        { this.props.fetchResponse &&
                        <PropertyListings
                            propertyValues={this.state.fetchResponseData}
                        />
                        }
            </div>
        );
    }

}


FetchResponse.propTypes = {

    FetchResponse: PropTypes.object

}