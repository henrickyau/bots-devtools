import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PropertyListings from './FetchRequestResponse/PropertyListings.js'

//import '../explorers.css';

import fetch_util from "./fetch_util";

export default class FetchRequest extends Component {
    constructor(props) {
        super(props);
        this.requestProperties = [
            {name: 'url', type: 'string'},
            {name: 'method', type: 'string'},
            {name: 'headers', type: 'function', resolver: fetch_util.constructFetchHeaders},
            {name: 'referrer', type: 'string'},
            {name: 'referrerPolicy', type: 'string'},
            {name: 'mode', type: 'string'},
            {name: 'credentials', type: 'string'},
            {name: 'redirect', type: 'string'},
            {name: 'integrity', type: 'string'},
            {name: 'cache', type: 'string'},
            {name: 'body', type: 'function', resolver: fetch_util.constructFetchBody},
        ];
        this.state = {
            fetchRequestData: fetch_util.getFetchData(this.props.fetchRequest, this.requestProperties)
        };

    }

    componentWillReceiveProps(nextProps) {
        /*if (!_.isEqual(this.props.properties, nextProps.properties)) {
            if (!_.isEqual(JSON.parse(this.state.propertiesStr), nextProps.properties)) {
                this.updatePropertiesStr(this.stringifyProperties(nextProps.properties));
            }
        }   */ 
        if (this.props.fetchRequest !== nextProps.fetchRequest) {
            this.setState({fetchRequestData: fetch_util.getFetchData(nextProps.fetchRequest, this.requestProperties)});
        }
    }

    render() {

        return (
            <div>
                        <h4>Request</h4>
                        { this.props.fetchRequest &&
                        <PropertyListings
                            propertyValues={this.state.fetchRequestData}
                        />
                        }
            </div>
        );
    }

}


FetchRequest.propTypes = {

    fetchRequest: PropTypes.object

}