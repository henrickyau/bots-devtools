const _ = require("underscore");

function computeChannelUrl(chatServerBaseUrl, channelId) {
  //var channelBaseUrl = (chatServerBaseUrl ? chatServerBaseUrl.replace(/\/$/, '')+'/bots' : '/ext/apps/chat/bots');
  //return channelBaseUrl + '/' + channelId;
  return computeChannelsUrl(chatServerBaseUrl) + "/" + channelId;
}

function computeChannelsUrl(chatServerBaseUrl) {
  if (chatServerBaseUrl) {
    let csUrl = chatServerBaseUrl.replace(/\/$/, "");
    if (csUrl.endsWith("/chat")) {
      return csUrl + "/bots";
    } else if (csUrl.endsWith("/bots")) {
      return csUrl;
    } else {
      return csUrl;
    }
  } else {
    return "/ext/apps/chat/bots";
  }
}

function computeUsersUrl(chatServerBaseUrl) {
  if (chatServerBaseUrl) {
    let csUrl = chatServerBaseUrl.replace(/\/$/, "");
    if (csUrl.endsWith("/chat")) {
      return csUrl + "/users";
    } else if (csUrl.endsWith("/bots")) {
      return csUrl.substr(0, csUrl.length-5)+ "/users";
    } else {
      return csUrl;
    }
  } else {
    return "/ext/apps/chat/users";
  }
}

function computeChatServerUrl(chatServerBaseUrl) {
  return computeChannelsUrl(chatServerBaseUrl);
}

function searchUsers(chatServerUrl) {
  return new Promise(function(resolve, reject) {
    let queriesRun = [];
    function processError(error, statusText) {
      console.log("Looks like there was a problem: " + error);
      resolve({
        status: false,
        queriesRun: queriesRun,
        messageError: statusText || error
      });
    }
    if (chatServerUrl) {
      let fetchUsersUrl = computeUsersUrl(chatServerUrl);
      fetch(fetchUsersUrl)
        .then(function(response) {
          queriesRun.push({
            resource: "USERS",
            url: fetchUsersUrl,
            type: "GET",
            status: response.status
          });
          if (response.status === 200) {
            response
              .json()
              .then(function(data) {
                resolve({
                  status: true,
                  queriesRun: queriesRun,
                  users: data
                });
              })
              .catch(function(err) {
                console.error("searchUsers responsejson error");
                processError(response.status, response.statusText);
              });
          } else {
            processError(response.status, response.statusText);
          }
        })
        .catch(function(err) {
          processError(err);
        });
    } else {
      processError("No chatserverurl");
    }
  });
}

function searchChannels(chatServerUrl, channelId) {
  return new Promise(function(resolve, reject) {
    let queriesRun = [];
    function processError(error, statusText) {
      console.log("Looks like there was a problem: " + error);
      resolve({
        status: false,
        queriesRun: queriesRun,
        messageError: statusText || error
      });
    }
    if (chatServerUrl) {
      let fetchChannelsUrl = computeChannelsUrl(chatServerUrl);
      fetch(fetchChannelsUrl, {mode: 'cors'})
        .then(function(response) {
          console.log(response);
          let lastQueryIndex = queriesRun.push({
            resource: "CHANNELS",
            url: fetchChannelsUrl,
            type: "GET",
            status: response.status
          }); 
          if (response.status === 200) {
            response
              .json()
              .then(function(data) {   
                if (channelId) {
                  let index = _.findIndex(data, function(channel) {
                    return channel.id === channelId;
                  });
                  if (index > -1) {
                    resolve({
                      status: true,
                      queriesRun: queriesRun,
                      channels: data,
                      channelIndex: index
                    });
                    return;
                  }
                }
                resolve({
                  status: true,
                  queriesRun: queriesRun,
                  channels: data
                });
              })
              .catch(function(err) {
                queriesRun[lastQueryIndex].errorDescription="response not understood";
                console.error("searchChannels responsejson error");
                processError('parse error', err);
              });
          } else {
            processError(response.status, response.statusText);
          }
        })
        .catch(function(err) {
          queriesRun.push({
            resource: "CHANNELS",
            url: fetchChannelsUrl,
            type: "GET",
            status: 'unknown',
            errorDescription: err
          }); 
          processError(err, err);
        });
    } else {
      reject("No chatserverurl");
    }
  });
}

function upsertChannel(chatServerUrl, channelId, channelData){
  return new Promise(function(resolve, reject) {
    let queriesRun = [];
    function processError(error, statusText) {
      console.log("Looks like there was a problem: " + error);
      resolve({
        status: false,
        queriesRun: queriesRun,
        messageError: statusText || error
      });
    }
    if (channelData 
        && channelId
        && channelData.channelName 
        && channelData.secretKey 
        && channelData.webhookUrl) {
      var body = {
        id: channelId,
        name: channelData.channelName,
        description: (channelData.description && channelData.description.length > 0 ? channelData.description : channelData.channelName),
        secretKey: channelData.secretKey,
        uri: channelData.webhookUrl,
        tenantId: 'chatbot-tenant',
        type: 'webhook'
      } 
      fetch(computeChannelUrl(chatServerUrl, channelId), {
            method: 'put',
            headers: {
                "Content-type": "application/json"
            },
            body: JSON.stringify(body)
        })
        //.then(json)
        .then(function (response) {
            console.log('Request processed with JSON response', response);
            if (response.status !== 201 && response.status !== 200) {
                processError(response.status, response.statusText);
                return;
            } else {
                console.log('Channel registered', channelId);
                resolve({
                  status: true,
                  queriesRun: queriesRun,
                  channelId: channelId
                });                                 
            }
        })
        .catch(function (err) {
            console.log('Request failed', err);
            processError(err);               
        });
    }
  });
}

function deleteChannel(chatServerUrl, channelId) {
  let queriesRun = [];
  return new Promise(function(resolve, reject) {
    function processError(error, statusText) {
      console.log("Looks like there was a problem: " + error);
      resolve({
        status: false,
        queriesRun: queriesRun,
        messageError: statusText || error
      });
    }
    if (chatServerUrl && channelId) {
      let deleteChannelUrl = computeChannelUrl(
        chatServerUrl,
        channelId
      );
      fetch(deleteChannelUrl, {
        method: "delete",
        headers: {
          "Content-type": "application/json"
        }
      })
        .then(function(response) {
          console.log(response);
          if (response.status === 204) {
            queriesRun.push({
              resource: "CHANNEL",
              url: deleteChannelUrl,
              type: "DELETE",
              status: response.status
            });
            searchChannels(chatServerUrl)
            .then(function(result) {
              queriesRun.concat(result.queriesRun);
              if (result.status) {
                resolve({
                  status: true,
                  queriesRun: queriesRun,
                  channels: result.channels
                });
              } else {
                resolve({
                  status: true,
                  queriesRun: queriesRun,
                  channels: []
                });
              }
            });
          } else {
            processError(
              response.status,
              response.statusText
            );
          }
        })
        .catch(function(err) {
          processError(err, err);
        });
    } else {
      reject("No chatserverurl or channelId");
    }
  });
}

function getQueryStatus(queriesRun, type, name){
    let description = '';
    if (queriesRun && queriesRun.length > 0){
      let queryResult = (_.find(queriesRun, function(queryRun){
        return (queryRun.type === type && queryRun.resource === name);
      }));
      if (queryResult){
        description = (function(q){
          return q.type + ' ' + q.url + ' returned ' + q.status + (q.errorDescription ? ' with error: '+q.errorDescription : '');
        })(queryResult);
      }
    }
    return description;
  }


module.exports = {
  computeChannelUrl: computeChannelUrl,
  computeChannelsUrl: computeChannelsUrl,
  computeChatServerUrl: computeChatServerUrl,
  searchChannels: searchChannels,
  searchUsers: searchUsers,
  upsertChannel: upsertChannel,
  deleteChannel: deleteChannel,
  getQueryStatus: getQueryStatus
};
