import React, { Component } from 'react';
import PropTypes from 'prop-types';

import WebsocketTester from '../../websocket/WebsocketTester.js';
import TesterProperties from '../../conversation/TesterProperties.js'

/*
 *  ChatServerTester component
 *  Synopsis:
 * 
 *  Props:
 *      Data:
 *        chatServerUrl
 *        channelId
 *        conversationProperties
 *        targetHeight
 *      Functions:
 * 
 *  State:
 *    wsState
 *    wsUrl
 *    userId
 *    historyConversation,
 *    testerHeight
 *    testerProperties
 * 
 *  UI Event Handlers:
 * 
 *  Lifecycle:
 *    componentDidUpdate:
 *      adjust tester height
 * 
 *  Requirements on Parent:
 * 
 *  Notes:
 * 
 *  To Do:
 */
export default class ChatServerTester extends Component {
  constructor(props) {
    super(props);
    this.setWsTesterState = this.setWsTesterState.bind(this);    
    this.setError = this.setError.bind(this);    
    this.setTesterProperties = this.setTesterProperties.bind(this);    

    this.state = {
      wsStateName: 'NOT OPEN',
      wsUrl: '',
      userId: Date.now().toString(),
      historyConversation: [],
      testerHeight: this.props.targetHeight,
      testerProperties: null
    };
  }

  setWsTesterState(wsState) {
    if (wsState){
      this.setState({
        wsUrl: wsState.wsUrl,
        wsStateName: wsState.wsStateName
      });
    } else {
      this.setState({
        wsUrl: "",
        wsState: "NOT OPEN"
      });
    }
  }

  setError(errorResponse){
    this.setState({
      errorResponse: errorResponse
    });
  }

  setTesterProperties(properties){
    this.setState({
      testerProperties: properties
    });
  }

  componentWillUnmount() {
  }

  componentDidUpdate() {
    console.log('ChatServerTester componentDidUpdate');
    if (this.statusArea) {
      let testerHeight = this.props.targetHeight - this.statusArea.clientHeight;
      if (this.state.testerHeight !== testerHeight) {
        console.log('ChatServerTester setState TesterHeight');
        this.setState({testerHeight: testerHeight});
      }
    }
  }

  render() {
    var testerBoxStyle = {
        borderWidth: '1px',
        borderStyle: 'ridge'
      };
    var statusBarStyle = {
        fontSize: '14px', 
        wordBreak: 'break-all', 
        padding: '18px',
        color: 'white'
    };
    var statusBarStyleNotOpen = Object.assign({}, statusBarStyle,{backgroundColor: '#e91e63b3'});
    var statusBarStyleOpen = Object.assign({}, statusBarStyle,{backgroundColor: '#31800085'});
    var statusBarStyleConnecting = Object.assign({}, statusBarStyle,{backgroundColor: '#ffc107c2'});
    
    
    var wsStatusBar = 
    <div style={(this.state.wsStateName==='OPEN' ? statusBarStyleOpen : (this.state.wsStateName==='CONNECTING' ? statusBarStyleConnecting : statusBarStyleNotOpen))}>
      <div>Websocket {this.state.wsStateName}: </div>
      <div style={{paddingLeft: '10px'}}>{this.state.wsUrl}</div>
    </div>;

    var botErrorBar = 
      <div style={statusBarStyleNotOpen}>
        <div>Bot Message ERROR </div>
        <div style={{paddingLeft: '10px'}}>{this.state.errorResponse}</div>
      </div>;

    return(
        <div style={testerBoxStyle}>
            <div ref={(statusArea) => { this.statusArea = statusArea; }}>
              { wsStatusBar }
              { this.state.errorResponse && botErrorBar }
            </div>
            <div>
              <div className='ChatServer-2-column-left' style={{
                borderRightStyle: 'dashed',
                borderRightColor: 'gray',
                borderRightWidth: '1px'
              }}>
                <WebsocketTester 
                  chatServerUrl={this.props.chatServerUrl}
                  channelId={this.props.channelId}
                  conversationProperties={this.props.conversationProperties}
                  userId={this.state.userId}
                  targetHeight={this.state.testerHeight}
                  initialConversation={this.state.historyConversation} 
                  returnWsTesterState={this.setWsTesterState}
                  setTesterProperties={this.setTesterProperties}
                  returnError={this.setError}
                />
              </div>
              <div className='ChatServer-2-column-right'>
                <TesterProperties
                  testerProperties={this.state.testerProperties}
                  targetHeight={this.state.testerHeight}/>
              </div>
            </div>
        </div>
    );
  }

}

ChatServerTester.propTypes = {

  chatServerUrl: PropTypes.string.isRequired,
  channelId: PropTypes.string.isRequired,
  conversationProperties: PropTypes.object.isRequired,
  targetHeight: PropTypes.number.isRequired
}
