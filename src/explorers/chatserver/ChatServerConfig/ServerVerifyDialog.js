import React from 'react';
import PropTypes from 'prop-types';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import NavigationClose from 'material-ui/svg-icons/navigation/close';

import UserListDialog from './UserListDialog.js';

import chatServerQueries from '../chat_server_queries.js';

/*
 *  ServerVerifyDialog component
 *  Synopsis:
 *    This component is a modal dialog that handles making connection to chatserver to determine if chatserver is valid or not.  Also can query for
 *    users sessions in chatserver.  Status and query details are returned to the parent.
 * 
 *  Props:
 *      Data:
 *        open
 *        chatServerUrl
 *      Functions:
 *        returnStatusSuccess
 *          chatServerUrl
 *          statusDescription
 *        returnStatusFailed
 *          chatServerUrl
 *          statusDescription
 *        handleClose
 * 
 *  State:
 *      basicAuthName
 *      basicAuthPassword
 *      messageError
 *      messageSuccess
 *      users
 *      showUsersDialogOpen
 * 
 *  UI Event Handlers:
 *    handleFieldChange
 *    handleButtonClose
 *    handleButtonCheck
 *    handleButtonUsers
 *    handleMessageErrorClose
 *    handleMessageSuccessClose
 * 
 *  Lifecycle:
 *    initComponent:
 *      check chat server status upon props.chatServerUrl
 * 
 *  Requirements on Parent:
 *  1. modal dialog could stay mounted while closed, or could be unmounted on close.  This is controlled by the parent component,
 * 
 *  Notes:
 *  1. chatServerUrl is an uncontrolled component.  Default value is props.chatServerUrl.  On check button press, the field value is read and
 *     then used for checking chat server status.  Upon success or failure, thhe chatServerUrl field value (changed or not) is sent back to parent.
 *  2. The uncontrolled form input value gets reset when the dialog is closed, so this.props.chatServerUrl is always the default value when opening
 *     the dialog.
 *  3. basicAuthName & basicAuthPassword not being used yet.
 *  4. The checking of chatserver is done by calling the /chat/bots endpoint (search channels).  Currently if the call returns 200 with any valid json,
 *     it is considered valud chatserver.  There is a new status endpoint of chatserver.  However, due to back compat, cannot well leverage the new endpoint 
 *     until later.
 * 
 *  To Do:
 *  1. Use chatserver /chat status endpoint to surely determine if chat server is legitimate
 *  2. Add support for basic Auth.  Server side needs work as well.
 *  3. Validate output of /chat/bots and /chat/users endpoints to make sure it's a chatserver
 *  4. server-side: remove expired users
 */

export default class ServerVerifyDialog extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
        basicAuthName: "",
        basicAuthPassword: "",
        messageError: "",
        messaegSuccess: "",
        users: null,
        showUsersDialogOpen: false
    };
    this.setShowUsersDialogClose = this.setShowUsersDialogClose.bind(this);    

    this.initComponent();
  }

  /** see comment on handleButtonCheck */
  initComponent() {
    let self = this;
    chatServerQueries.searchChannels(this.props.chatServerUrl)
    .then(function(result){
      if (result.status) {
        self.props.returnStatusSuccess(self.props.chatServerUrl, chatServerQueries.getQueryStatus(result.queriesRun, 'GET', 'CHANNELS'));
      } else {
        self.setMessageError(result.messageError);
        self.props.returnStatusFailed(self.props.chatServerUrl, chatServerQueries.getQueryStatus(result.queriesRun, 'GET', 'CHANNELS'));        
      }
    });
  }

  /** UI Event Handlers */
    handleMessageErrorClose = (event) => {
      this.clearMessageError();
    }
  
    handleMessageSuccessClose = (event) => {
      this.clearMessageSuccess();
    }

  /**
   * Fetch the chat server (by leveraging searchChannels but not passing channelId) and decide if it's a valid chat server.  
   * Returns a promise with success or failure outcome.
   * If chat server is accessible, 
   *    self.props.returnStatusSuccess(chatServerUrl, statusDescription);
   * If chat server is not accessible, 
   *    self.props.returnStatusFailed(chatServerUrl, statusDescription);
   */
    handleButtonCheck = (event) =>  {
      let self = this;
      if (this.chatServerUrl.input.value) {
        chatServerQueries.searchChannels(this.chatServerUrl.input.value)
        .then(function(result){
          if (result.status) {
            self.clearMessages();
            self.props.returnStatusSuccess(self.chatServerUrl.input.value, chatServerQueries.getQueryStatus(result.queriesRun, 'GET', 'CHANNELS'));
            self.props.handleClose();
          } else {
            self.setMessageError(result.messageError);
            self.clearMessageSuccess();
            self.props.returnStatusFailed(self.chatServerUrl.input.value, chatServerQueries.getQueryStatus(result.queriesRun, 'GET', 'CHANNELS'));        
          }
        });  
      }
    };

    handleButtonUsers = (event) =>  {
      let self = this;
      if (this.chatServerUrl.input.value) {
        chatServerQueries.searchUsers(this.chatServerUrl.input.value)
        .then(function(result){
          if (result.status) {
            if (result.status) {
              self.clearMessages();
              self.setState({
                users: result.users,
                showUsersDialogOpen: true                
              });
            } else {
              self.setMessageError(result.messageError);
              self.clearMessageSuccess();
              self.setState({
                users: null,
                showUsersDialogOpen: false                
              });
            }
          } else {
            self.setMessageError(result.messageError);
            self.clearMessageSuccess();
            self.props.returnStatusFailed(self.chatServerUrl.input.value, result.statusDescription);        
          }
        });  
      }
    };

    handleButtonClose = (event) =>  {
      this.clearMessages();
      this.props.handleClose();
    };
  
    handleFieldChange = (event) => {
      let state = {};
      state[event.target.name] = event.target.value;
      this.setState(state);
    }
  
  
  /** Component Methods */
  setUsers(users) {
    this.setState({users: users});
  }

  setShowUsersDialogClose() {
    this.setState({showUsersDialogOpen: false});
  };

  setMessageError(msg) {
    this.setState({messageError: msg});
  }

  setMessageSuccess(msg) {
    this.setState({messageSuccess: msg});
  }

  clearMessageError(){
    this.setState({messageError: ""});    
  }

  clearMessageSuccess(){
    this.setState({messageSuccess: ""});    
  }

  clearMessages(){
    this.clearMessageError();
    this.clearMessageSuccess();    
  }

  componentWillUpdate(nextProps, nextState) {
    console.debug("ChatServerCheckDialog componentWillUpdate", nextProps.chatServerUrl);
    console.debug("chatServerUrl input", (this.chatServerUrl ? this.chatServerUrl.input.value : "not set"));
  }

  componentDidUpdate(prevProps, prevState) {
    console.debug("ChatServerCheckDialog componentDidUpdate", prevProps.chatServerUrl);
    console.debug("chatServerUrl input", (this.chatServerUrl ? this.chatServerUrl.input.value : "not set"));
  }

  /** Render Method */
  render() {

    const actions = [
      <FlatButton
        label="Close"
        primary={false}
        onClick={this.handleButtonClose}
      />,
      <FlatButton
        label="Users"
        secondary={true}
        onClick={this.handleButtonUsers}
      />,
      <FlatButton
        label="Check"
        primary={true}
        onClick={this.handleButtonCheck}
      />,
    ];

    var messageErrorBar = 
    <AppBar 
        style={{backgroundColor: 'red'}} 
        title={'Error: '+this.state.messageError} 
        titleStyle={{fontSize: '14px'}}
        iconElementLeft={<IconButton onClick={this.handleMessageErrorClose}><NavigationClose /></IconButton>}
      />;

    var messageSuccessBar = 
    <AppBar title={'Success: '+this.state.messageSuccess} iconElementLeft={<IconButton onClick={this.handleMessageSuccessClose}><NavigationClose /></IconButton>}/>;

    return (

        <Dialog
          title="Check Chat Server Status"
          actions={actions}
          modal={true}
          open={this.props.open}
          autoScrollBodyContent={true}
        >
            <div>
                {this.state.messageSuccess && messageSuccessBar
                }
                {this.state.messageError && messageErrorBar
                }
                <p>
                  Use the chatserver baseUrl that ends with '/chat'.
                </p>
                <TextField name="chatServerUrl" defaultValue={this.props.chatServerUrl} floatingLabelText="Chat Server URL" fullWidth={true} ref={(input) => { this.chatServerUrl = input; }}/>                
                <br/>
                <TextField name="basicAuthName" floatingLabelText="Basic Authentication Name" fullWidth={true} value={this.state.basicAuthName} onChange={this.handleFieldChange}/>
                <br/>                    
                <TextField name="basicAuthPassword" floatingLabelText="Basic Authentication Password" fullWidth={true} value={this.state.basicAuthPassword} onChange={this.handleFieldChange}/>
                <br/>                    
            </div>
            { this.state.showUsersDialogOpen &&
          <UserListDialog 
              handleClose={this.setShowUsersDialogClose} 
              open={this.state.showUsersDialogOpen}
              users={this.state.users}/>
          }
        </Dialog>
    );
  }
}

ServerVerifyDialog.propTypes = {

  chatServerUrl: PropTypes.string.isRequired,
  open: PropTypes.bool,

  handleClose: PropTypes.func.isRequired,
  returnStatusSuccess: PropTypes.func.isRequired,
  returnStatusFailed: PropTypes.func.isRequired
}
