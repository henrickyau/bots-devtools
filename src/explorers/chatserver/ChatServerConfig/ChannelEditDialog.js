import React from 'react';
import PropTypes from 'prop-types';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import NavigationClose from 'material-ui/svg-icons/navigation/close';

import _ from 'underscore';
import chatServerQueries from '../chat_server_queries.js';

/*
 *  ChannelEditDialog component
 *  Synopsis:
 *    This component is a modal dialog that edits or creates a new chatserver channel
 *
 *  Props:
 *    Data:
 *      chatServerUrl (required)
 *      channelData (optional)
 *    Functions:
 *      handleClose
 *      returnChannelId (channelId)
 * 
 *  State:
 *    channelId
 *    channelName
 *    description
 *    secretKey
 *    webhookUrl
 *    error
 *    success
 * 
 *  Child Components:
 * 
 * 
 *  Lifecycle:
 */
export default class ChannelEditDialog extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
        channelId: this.props.channelData.id,
        channelName: this.props.channelData.name,
        description: this.props.channelData.description,
        secretKey: this.props.channelData.secretKey,
        webhookUrl: this.props.channelData.uri,
        error: "",
        success: ""
    };
  }
 
  handleMessageErrorClose = (event) => {
    this.setState({error: ""});
  }

  handleMessageSuccessClose = (event) => {
    this.setState({success: ""});
  }

  handleButtonSubmit = (event) => {
    var self = this;
    chatServerQueries.upsertChannel(this.props.chatServerUrl, this.channelIdField.input.value, _.omit(this.state,['error', 'success']))
    .then(function(result){
      if (result.status) {
        self.setState({success: 'Channel ID '+result.channelId + ' registered',
                       error: ''});
        self.props.returnChannelId(result.channelId);
      } else {
        self.setState({error: result.messageError,
          success: ''});
      }  
    })
    .catch(function(err){
      self.setState({error: err,
        success: ''});
    });
  }

  handleButtonClose = () => {
    this.props.handleClose();
    this.handleMessageErrorClose();
    this.handleMessageSuccessClose();
  };

  handleButtonContinue = () => {
    var self = this;
    chatServerQueries.searchChannels(this.props.chatServerUrl, this.channelIdField.input.value)
    .then(function(result){
      self.setState({
        channelId: self.channelIdField.input.value});
      if (result.status && result.channelIndex >= 0){
          let channelData = result.channels[result.channelIndex];
          self.setState({
            channelId: channelData.id,
            channelName: channelData.name,
            description: channelData.description,
            secretKey: channelData.secretKey,
            webhookUrl: channelData.uri
          });
      } 
    })
    .catch(function(err){
      self.setState({error: err,
        success: ''});
    });
  };


  handleChangeField = (event) => {
    let state = {};
    state[event.target.name] = event.target.value;
    this.setState(state);
  }

  //================
  render() {

    const actionsWithId = [
      <FlatButton
        label="Close"
        primary={false}
        onClick={this.handleButtonClose}
      />,
      <FlatButton
        label="Register"
        primary={true}
        onClick={this.handleButtonSubmit}
        disabled={!this.state.channelId || !this.state.channelName || !this.state.secretKey || !this.state.webhookUrl}
      />,
    ];

    const actionsWithoutId = [
      <FlatButton
        label="Close"
        primary={false}
        onClick={this.handleButtonClose}
      />,
      <FlatButton
        label="Continue"
        primary={true}
        onClick={this.handleButtonContinue}
      />,
    ];

    var errorBar = 
    <AppBar 
      style={{backgroundColor: 'red'}} 
      title={'Error: '+this.state.error} 
      titleStyle={{fontSize: '14px'}}
      iconElementLeft={<IconButton onClick={this.handleMessageErrorClose}><NavigationClose /></IconButton>}/>;

    var successBar = 
    <AppBar 
      title={'Success: '+this.state.success} 
      titleStyle={{fontSize: '14px'}}
      iconElementLeft={<IconButton onClick={this.handleMessageSuccessClose}><NavigationClose /></IconButton>}/>;

    return (

        <Dialog
          title="Register/Edit Webhook Channel"
          actions={(this.state.channelId ? actionsWithId : actionsWithoutId)}
          modal={true}
          open={this.props.open}
          autoScrollBodyContent={true}
        >
            <div>
                {this.state.success && successBar
                }
                {this.state.error && errorBar
                }
                <TextField name="channelId" floatingLabelText="Channel ID" defaultValue={this.state.channelId} disabled={!!this.state.channelId} fullWidth={true} ref={(input) => { this.channelIdField = input; }}/>
                <br/>
                <div style={{visibility: (this.state.channelId ? 'visible' : 'hidden')}}>
                  <TextField name="channelName" floatingLabelText="Name" fullWidth={true} value={this.state.channelName} onChange={this.handleChangeField}/>
                  <br/>
                  <TextField name="secretKey" floatingLabelText="Secret Key" fullWidth={true} value={this.state.secretKey} onChange={this.handleChangeField}/>
                  <br/>
                  <TextField name="webhookUrl" floatingLabelText="Webhook URL" fullWidth={true} value={this.state.webhookUrl} onChange={this.handleChangeField}/>
                </div>
            </div>
        </Dialog>
    );
  }
}

ChannelEditDialog.propTypes = {

  chatServerUrl: PropTypes.string.isRequired,
  open: PropTypes.bool,
  channelData: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    description: PropTypes.string,
    secretKey: PropTypes.string,
    uri: PropTypes.string
  }),

  handleClose: PropTypes.func.isRequired,
  returnChannelId: PropTypes.func.isRequired
}
