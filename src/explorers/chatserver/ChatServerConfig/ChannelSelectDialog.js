import React from "react";
import PropTypes from 'prop-types';
import Dialog from "material-ui/Dialog";
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn
} from "material-ui/Table";
import FlatButton from "material-ui/FlatButton";
import AppBar from "material-ui/AppBar";
import IconButton from "material-ui/IconButton";
import NavigationClose from "material-ui/svg-icons/navigation/close";

import ChannelEditDialog from './ChannelEditDialog.js';

const chatServerQueries = require('../chat_server_queries.js');
const _ = require('underscore');


/*
 *  ChannelSelectDialog component
 *  Props:
 *      Data:
 *        open
 *        chatServerUrl
 *        channelId
 *      Functions:
 *        returnChannelSelection
 *          channelId
 *          channelName
 *          channelStatus
 *          channelStatusText
 *        handleClose
 * 
 *  State:
 *      channels
 *      selected
 *      messageError
 *      messageSuccess
 *      registerDialogOpen
 * 
 *  UI Event Handlers:
 *    handleFieldChange
 *    handleButtonDelete
 *    handleButtonClose
 *    handleButtonEdit
 *    handleButtonAdd
 *    handleButtonRefresh
 *    handleButtonSelect
 *    handleMessageErrorClose
 *    handleMessageSuccessClose
 *    handleRowSelection
 * 
 *  Lifecycle:
 *    initComponent:
 *      check chat server status upon props.chatServerUrl
 * 
 *  Requirements on Parent:
 *  1. modal dialog could stay mounted while closed, or could be unmounted on close.  This is controlled by the parent component,
 * 
 *  Notes:
 * 
 *  To Do:
 */

export default class ChannelSelectDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      //channels: [],
      selected: [],
      messageSuccess: "",
      messageError: "",
      registerDialogOpen: false,      
      editDialogOpen: false      
    };
    this.returnChannelRegisterSuccess = this.returnChannelRegisterSuccess.bind(this);    

    console.log("chatserverselectchanneldialog constructor");
    this.initComponent();
  }

  initComponent(){
    //this.searchChannels(this.props.chatServerUrl, this.props.channelId);
    /*let channelObj = this.getChannelIndex(this.props.channelId);
    if (channelObj) {
      this.state.selected = [channelIndex];
    }*/
    //this.state.selected = [this.getChannelIndex(this.props.channelId)];
  }

  /** UI Handlers */
  handleButtonDelete = (event) => {
    let self = this;
    this.clearMessages();
    if (this.state.selected && this.state.selected.length === 1) {
      let channelId = this.props.channels[this.state.selected[0]].id;
      this.props.deleteChannel(channelId)
      .then(function(result){
        console.debug('ButtonDelete', result);
        if (result.status) {
          self.setState({
            messageSuccess: "Channel ID " + channelId + " deleted",
            messageError: "",
            selected: []
          });
          //self.clearMessageError();
          //self.setMessageSuccess("Channel ID " + channelId + " deleted");
          //self.setChannelsAndSelection(result.channels, result.channelIndex);   
          //self.returnChannelSelection("", "", 1, "");
        } else {
          //self.clearChannelsAndSelection();
          //self.returnChannelSelection("", "", 0, "");        
        }  
      })
      .catch(function(err){
        console.error(err);
      });
    } 
  }

  handleButtonAdd = (event) => {
    this.clearMessages();
    this.clearSelection();
    this.openRegisterDialog();
  }

  handleButtonEdit = (event) => {
    this.clearMessages();
    if (this.state.selected && this.state.selected.length === 1) {
      this.openEditDialog();
    } 
  }

  handleButtonClose = (event) => {
    this.clearMessages();
    this.props.handleClose();
  }

  handleButtonRefresh = (event) => {
    this.props.refreshChannels(this.props.chatServerUrl, this.props.channelId);
  }

  /*handleButtonRefresh = (event) => {
    var self = this;
    chatServerQueries.searchChannels(this.props.chatServerUrl, this.props.channelId)
    .then(function(result){
      console.debug('ButtonRefresh', result);
      if (result.status) {
        self.setChannelsAndSelection(result.channels, result.channelIndex);   
        if (result.channelIndex > -1){
          self.returnChannelSelection(result.channels[result.channelIndex].id, result.channels[result.channelIndex].name, 1, "");
        } else {
          self.returnChannelSelection("", "", 1, "");
        }
      } else {
        self.clearChannelsAndSelection();
        self.returnChannelSelection("", "", 0, "");        
      }  
    })
    .catch(function(err){
      console.error(err);
    });
  }*/

  handleButtonSelect = (event) => {
    this.clearMessages();
    if (this.state.selected && this.state.selected.length===1){
      this.returnChannelSelection(this.props.channels[this.state.selected[0]].id, 
        this.props.channels[this.state.selected[0]].name, 1, "works");
    }
    this.props.handleClose();
  }

  handleMessageErrorClose = (event) => {
    this.clearMessageError();
  }

  handleMessageSuccessClose = (event) => {
    this.clearMessageSuccess();
  }

  handleRowSelection = (selectedRows) => {
    this.setState({
      selected: selectedRows
    });
  };

  /** Component Methods */
  getChannelIndex(channelId){
    return _.findIndex(this.props.channels, function(channel) {
      return channel.id === channelId;
    });
  }

  setMessageError(msg) {
    this.setState({messageError: msg});
  }

  setMessageSuccess(msg) {
    this.setState({messageSuccess: msg});
  }

  clearMessageError(){
    this.setState({messageError: ""});    
  }

  clearMessageSuccess(){
    this.setState({messageSuccess: ""});    
  }

  clearMessages(){
    this.clearMessageError();
    this.clearMessageSuccess();    
  }

  clearSelection(){
    if (this.state.selected && this.state.selected.length === 1) {
      this.setState({
        selected: []
      });
    } 
  }

  setChannelSelected(){
    if (this.props.channelId) {
      if (this.props.channels) {
        if (!this.state.selected || this.state.selected.length === 0){
          this.setState({selected: [this.getChannelIndex(this.props.channelId)]});
        } else if (this.props.channels[this.state.selected[0]].id !== this.props.channelId) {
          this.setState({selected: [this.getChannelIndex(this.props.channelId)]});
        }
      } else if (this.state.selected && this.state.selected.length > 0) {
        this.clearSelection();
      }
    } else if (this.state.selected && this.state.selected.length > 0) {
      this.clearSelection();
    }
    //this.state.selected = [this.getChannelIndex(this.props.channelId)];
  }

  setChannelsAndSelection(channels, channelIndex){
    this.setState({
      channels: channels,
      selected: (channelIndex && channelIndex >= 0 ? [channelIndex] : []),
    }); 
  }

  clearChannelsAndSelection() {
    this.setState({
      channels: [],
      selected: []
    });
  }

  openRegisterDialog(){
    this.setState({
      registerDialogOpen: true
    });
  }

  openEditDialog(){
    this.setState({
      editDialogOpen: true
    });
  }

  returnChannelSelection(channelId, channelName, channelStatus, statusDescription) {
      this.props.returnChannelSelection(channelId, channelName, channelStatus, statusDescription);
  }

  returnChannelRegisterSuccess(channelId) {
    let self = this;
    chatServerQueries.searchChannels(this.props.chatServerUrl, channelId)
    .then(function(result){
      console.debug('RegisterSuccess', result);
      if (result.status) {
        self.setChannelsAndSelection(result.channels, result.channelIndex);   
        if (result.channelIndex > -1){
          self.returnChannelSelection(result.channels[result.channelIndex].id, result.channels[result.channelIndex].name, 1, "");
        } else {
          self.returnChannelSelection("", "", 1, "");
        }
      } else {
        self.clearChannelsAndSelection();
        self.returnChannelSelection("", "", 0, "");        
      }  
    })
    .catch(function(err){
      console.error(err);
    });
  }

  isSelected = index => {
    return this.state.selected.indexOf(index) !== -1;
  };

  /****** Register Dialog */
  handleRegisterDialogClose = () => {
    this.setState({registerDialogOpen: false, editDialogOpen: false});
  };

  componentDidUpdate(prevProps, prevState) {
    console.log('chatserverselectchanneldialog component did update', this.props.chatServerUrl, this.props.channelId);
    if (_.difference(prevProps.selected, this.state.selected).length === 0 &&
        _.difference(this.state.selected, prevProps.selected).length === 0) {
          this.setChannelSelected();
    }
  }

  render() {
    const actions = [
      <FlatButton label="Close" primary={false} onClick={this.handleButtonClose} />,
      <FlatButton label="Refresh" primary={false} onClick={this.handleButtonRefresh} />,
      <FlatButton
        label="New"
        secondary={true}
        onClick={this.handleButtonAdd}
      />,
      <FlatButton
        label="Edit"
        secondary={true}
        onClick={this.handleButtonEdit}
        disabled={this.state.selected.length === 0}
      />,
      <FlatButton
        label="Delete"
        secondary={true}
        onClick={this.handleButtonDelete}
        disabled={this.state.selected.length === 0}
      />,
      <FlatButton label="Select" 
        primary={true} 
        onClick={this.handleButtonSelect} 
        disabled={this.state.selected.length === 0}
      />
    ];

    var messageErrorBar = 
    <AppBar 
      style={{backgroundColor: 'red'}} 
      title={'Error: '+this.state.messageError} 
      titleStyle={{fontSize: '14px'}}
      iconElementLeft={<IconButton onClick={this.handleMessageErrorClose}><NavigationClose /></IconButton>}/>;

    var messageSuccessBar = 
    <AppBar 
      title={'Success: '+this.state.messageSuccess} 
      titleStyle={{fontSize: '14px'}}
      iconElementLeft={<IconButton onClick={this.handleMessageSuccessClose}><NavigationClose /></IconButton>}/>;


    const tableRows = this.props.channels.map((channel, index) => (
      <TableRow selected={this.isSelected(index)} key={index}>
        <TableRowColumn>{channel.id}</TableRowColumn>
        <TableRowColumn>{channel.name}</TableRowColumn>
      </TableRow>
    ));

    return (
      <Dialog
        title="Select Channel"
        actions={actions}
        modal={true}
        open={this.props.open}
        autoScrollBodyContent={true}
      >
        <div>
          {this.state.messageSuccess && messageSuccessBar}
          {this.state.messageError && messageErrorBar}
          <p>Select from the list of registered channels.</p>
          <Table onRowSelection={this.handleRowSelection}>
            <TableHeader
              displaySelectAll={false}
              adjustForCheckbox={false}
              enableSelectAll={false}
            >
              <TableRow>
                <TableHeaderColumn>Channel ID</TableHeaderColumn>
                <TableHeaderColumn>Name</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false} deselectOnClickaway={false}>
              {tableRows}
            </TableBody>
          </Table>
          { (this.state.registerDialogOpen || this.state.editDialogOpen) &&
          <ChannelEditDialog 
              chatServerUrl={this.props.chatServerUrl}
              handleClose={this.handleRegisterDialogClose} 
              returnChannelId={this.returnChannelRegisterSuccess} 
              open={(this.state.registerDialogOpen || this.state.editDialogOpen)}
              channelData={(this.state.editDialogOpen ? 
                              (this.state.selected.length > 0 ? this.props.channels[this.state.selected[0]] : {}) : {})}/>
          }
        </div>
      </Dialog>
    );
  }
}

ChannelSelectDialog.propTypes = {

  chatServerUrl: PropTypes.string.isRequired,
  open: PropTypes.bool,
  channelId: PropTypes.string,

  handleClose: PropTypes.func.isRequired,
  returnChannelSelection: PropTypes.func.isRequired
}
