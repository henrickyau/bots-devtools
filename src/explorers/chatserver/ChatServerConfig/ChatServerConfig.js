import React from 'react';
import PropTypes from 'prop-types';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

import ServerVerifyDialog from './ServerVerifyDialog.js';
import ChannelSelectDialog from './ChannelSelectDialog.js';
import JSONProperties from '../../common/JSONProperties.js'

import StatusSuccess from 'material-ui/svg-icons/action/check-circle';
import StatusFailed from 'material-ui/svg-icons/action/highlight-off';
import {red500, green500} from 'material-ui/styles/colors';

import chatServerQueries from '../chat_server_queries.js';
import _ from 'underscore';

import configure_step2 from './configure_step2';
import '../chat-server.css';

/*
 *  ChatServerConfig component
 *  Props:
 *    Data:
 *      chatConfig
 *    Functions:
 *      returnChatConfig(chatConfig)
 *        chatServerUrl
 *        channelId
 *        conversationProperties
 *        userId
 * 
 *  State:
 *    chatServerUrl
 *    channelId
 *    conversationProperties
 *    userId
 *
 *    chatServerStatus
 *    chatServerStatusText
 *    channelStatus
 *    channelStatusText
 *    channelName
 *
 *    chatServerDialogOpen
 *    channelDialogOpen
 * 
 *  UI Event Handlers:
 *    Chat Server:
 *      handleButtonChatServerCheck
 * 
 *  Child Components:
 *    ServerVerifyDialog
 *      Data:
 *        open
 *        chatServerUrl
 *      Functions:
 *        returnStatusSuccess
 *          chatServerUrl
 *          statusDescription
 *        returnStatusFailed
 *          chatServerUrl
 *          statusDescription
 *        handleClose
 *      Lifecycle: Keep mounted
 * 
 *    ChannelSelectDialog
 *      Data:
 *        open={this.state.channelDialogOpen}
 *        chatServerUrl={this.state.chatServerUrl}
 *        channelId={this.state.channelId}
 *      Functions:
 *        handleSelect={this.handleSelectSelect}
 *        handleClose={this.handleSelectDialogClose}
 *      Lifecycle: Unmount on close
 * 
 *  Lifecycle:
 *    componentDidUpdate:
 *      if chatServerUrl or channelId changed, and channelId is not null
 *        searchChannel
 */


export default class ChatServerConfig extends React.Component {

  constructor(props) {
    super(props);

    this.chatConfigProperties = [
      'chatServerUrl',
      'channelId',
      'conversationProperties',
      'userId'
    ];
    this.state = Object.assign({}, this.props.chatConfig, 
                                    {
                                      chatServerStatus: '',
                                      chatServerStatusText: '',
                                      channelStatus: '',
                                      channelStatusText: '',
                                      channelName: '',
                                      //conversationProperties: {},

                                      chatServerDialogOpen: false, 
                                      channelDialogOpen: false,

                                      channelList: []
                                    });

    this.setChatServerStatusSuccess = this.setChatServerStatusSuccess.bind(this);    
    this.setChatServerStatusFailed = this.setChatServerStatusFailed.bind(this);    
    this.setChatServerDialogClose = this.setChatServerDialogClose.bind(this);

    this.setChannelDialogClose = this.setChannelDialogClose.bind(this);
    this.setChannelSelection = this.setChannelSelection.bind(this);
    this.refreshChannels = this.refreshChannels.bind(this);
    this.deleteChannel = this.deleteChannel.bind(this);

    this.setConversationProperties = this.setConversationProperties.bind(this);

    this.initComponent();
  }

  initComponent() {
    this.validateChannel(this.state.chatServerUrl, this.state.channelId);
  }

  validateChannel(chatServerUrl, channelId){
    let self = this;
    if (chatServerUrl) {
        chatServerQueries.searchChannels(chatServerUrl, channelId)
        .then(function(result){
          console.debug('validateChannel', result);
          if (result.status) {
            self.setChannelList(result.channels);
            if (result.channelIndex > -1){
              self.setChannelSelection(
                result.channels[result.channelIndex].id, 
                result.channels[result.channelIndex].name, 
                1, 
                'Channel ID '+result.channels[result.channelIndex].id+ ' found.');
            } else {
              if (channelId) {
                self.setChannelSelection(channelId, "UNKNOWN", -1, 'Channel ID '+channelId+ ' not found.');
              } else {
                self.setChannelSelection("", "", 0, "");                         
              }
            }
          } else {
            self.setChannelList([]);
            self.setChannelSelection(channelId, "UNKNOWN", -1, chatServerQueries.getQueryStatus(result.queriesRun, 'GET', 'CHANNELS'));
          }
        });  
    } else {
      self.setChannelSelection("", "", 0, ""); 
    } 
  }

  deleteChannel(channelId){
    let self = this;
    return new Promise(function(resolve, reject) {
      chatServerQueries.deleteChannel(self.state.chatServerUrl, channelId)
      .then(function(result){
        if (result.status) {
          self.setChannelList(result.channels);
        } else {
          self.setChannelList([]);        
        }
        resolve(result);
      });
    });
  }

  /** UI Event Handlers */
  handleButtonChatServerCheck = () => {
    this.setState({chatServerDialogOpen: true});
  };

  handleFieldChatServerUrl = (event) => {
    this.setState({chatServerStatus: 0, chatServerUrl: event.target.value, chatServerStatusText: ''},
      function(){
        this.returnChatConfig();
      });
  };

  /** Component Methods */
  setChatServerDialogClose() {
    this.setState({chatServerDialogOpen: false});
  };

  returnChatConfig() {
    let config = _.pick(this.state, this.chatConfigProperties);
    console.log(this.state);
    config = _.omit(config, _.filter(_.keys(config), function(key){
      return !config[key];
    }));
    console.log('chatConfig', config);
    this.props.returnChatConfig(config);
  }

  setChatServerStatusSuccess(chatServerUrl, statusText) {
    this.setState({chatServerStatus: 1, chatServerUrl: chatServerUrl, chatServerStatusText: statusText},
      function(){
        this.returnChatConfig();
      });
  };

  setChatServerStatusFailed(chatServerUrl, statusText) {
    this.setState({chatServerStatus: -1, chatServerUrl: chatServerUrl, chatServerStatusText: statusText}, 
      function(){
        this.returnChatConfig();
      });
  };

  //=========
  handleButtonChannelLookup = () => {
    this.setState({
        channelDialogOpen: true
      });
  };

  setChannelDialogClose(){
    this.setState({channelDialogOpen: false});
  };

  setChannelList(channelList) {
    this.setState({
      channelList: channelList
    });
  }

  setChannelSelection = function(channelId, channelName, channelStatus, channelStatusText) {
    //this.setState({channelDialogOpen: false});
    console.debug('setChannelSelection', channelId);
    let self = this;
    this.setState({
        channelId: channelId,
        channelName: channelName,
        channelStatus: channelStatus,
        channelStatusText: channelStatusText
    }, function(){
        self.returnChatConfig();
    });
  };

  refreshChannels(chatServerUrl, channelId) {
    this.validateChannel(chatServerUrl, channelId);
  }

  //=========

  setConversationProperties(properties){
      let self = this;
        this.setState({
          conversationProperties: properties
        }, function(){
          self.returnChatConfig();
        });
  };

  componentDidUpdate(prevProps, prevState) {
      console.log('chatserverconfig component did update', this.state.chatServerUrl, this.state.channelId);
      if (this.state.channelId !== prevState.channelId || this.state.chatServerUrl !== prevState.chatServerUrl){
        this.validateChannel(this.state.chatServerUrl, this.state.channelId);
      }
  }

  render() {

    var chatServerStatus;
    switch(this.state.chatServerStatus) {
        case 1:     
            chatServerStatus = <span><StatusSuccess color={green500} viewBox="-5 -5 36 36" style={{width: '36px', height: '36px', verticalAlign: 'middle'}}/><span style={{color: 'rgba(0, 0, 0, 0.3)'}}>{this.state.chatServerStatusText}</span></span>;
            break;
        case -1:
            chatServerStatus = <span><StatusFailed color={red500} viewBox="-5 -5 36 36" style={{width: '36px', height: '36px', verticalAlign: 'middle'}}/><span style={{color: 'rgba(0, 0, 0, 0.3)'}}>{this.state.chatServerStatusText}</span></span>;
            break;
        default:
            chatServerStatus = <span/>
    };

    var channelStatus;
    switch(this.state.channelStatus) {
        case 1:     
            channelStatus = <span><StatusSuccess color={green500} viewBox="-5 -5 36 36" style={{width: '36px', height: '36px', verticalAlign: 'middle'}}/><span style={{color: 'rgba(0, 0, 0, 0.3)'}}>{this.state.channelStatusText}</span></span>;
            break;
        case -1:
            channelStatus = <span><StatusFailed color={red500} viewBox="-5 -5 36 36" style={{width: '36px', height: '36px', verticalAlign: 'middle'}}/><span style={{color: 'rgba(0, 0, 0, 0.3)'}}>{this.state.channelStatusText}</span></span>;
            break;
        default:
            channelStatus = <span/>
    };
    let channelDisplay = (this.state.channelId ? this.state.channelName +" (ID:"+this.state.channelId+")" : "");

    return (
      <div className="ChatServer">
        <div>
          <h3>1. Check if Chat Server is up and running.</h3>
          <p>
            Put in the chat server URL (ends with /chat/bots), for example: 
            <br/>
            <span style={{color: 'blue', fontStyle: 'italic', paddingLeft: '20px'}}>http://localhost:8888/ext/apps/chat/bots</span>
          </p>
          <div>
            <TextField name="chatServerUrl" floatingLabelText="Chat Server URL" fullWidth={true} value={this.state.chatServerUrl} onChange={this.handleFieldChatServerUrl}/>
            <br/>
            <RaisedButton label="Check" onClick={this.handleButtonChatServerCheck} style={{verticalAlign: 'middle'}}/>
              &nbsp;
            { this.state.chatServerUrl && chatServerStatus}
            <br/>
            <ServerVerifyDialog 
              open={this.state.chatServerDialogOpen}
              chatServerUrl={this.state.chatServerUrl}
              returnStatusSuccess={this.setChatServerStatusSuccess}
              returnStatusFailed={this.setChatServerStatusFailed}
              handleClose={this.setChatServerDialogClose}
            />
          </div>
        </div>   
        <br/>         
        {configure_step2}
        <br/>         
        <div>
          <h3>3. Register Webhook Channel with the Chat Server</h3>
          <p>
            Use the channel_id in the step above.  Register the webhook channel details with the Chat Server:
          </p>
          <div>
            <TextField hintText="Channel" floatingLabelText="Channel" fullWidth={true} value={channelDisplay} disabled={false}/>
            <br/>
            <RaisedButton label="Lookup" onClick={this.handleButtonChannelLookup} />
            &nbsp;
            { this.state.channelId && channelStatus}
            <br/>
            <ChannelSelectDialog 
              chatServerUrl={this.state.chatServerUrl}
              returnChannelSelection={this.setChannelSelection} 
              handleClose={this.setChannelDialogClose} 
              open={this.state.channelDialogOpen}
              refreshChannels={this.refreshChannels}
              deleteChannel={this.deleteChannel}
              channels={this.state.channelList}
              channelId={this.props.chatConfig.channelId}/>
          </div>
        </div>
        <br/>         
        <div>
          <h3>4. Send profile properties to customize the conversation</h3>
          <p>
            Properties can be customized and sent along:
          </p>
          <JSONProperties 
                    label={"Profile properties JSON"}
                    properties={this.state.conversationProperties}
                    maxRows={20}
                    returnProperties={this.setConversationProperties}
          />
        </div>
      </div>


    );
  }
}

ChatServerConfig.propTypes = {

  chatConfig: PropTypes.shape({
    chatServerUrl: PropTypes.string,
    channelId: PropTypes.string,
    conversationProperties: PropTypes.object,
    userId: PropTypes.string
  }),

  returnChatConfig: PropTypes.func.isRequired
}
