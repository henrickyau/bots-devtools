import React from 'react';

var content = (
    <div>
        <h3>2. Create a Webhook Channel for the Bot</h3>
        <p>
            On the Bots UI, create a webhook channel for the Bot.
        </p>
        <p>
            Configure the Outgoing Webhook URI to the chat server webhook receiving endpoint on HTTPS.  The URI is in the form:
            <br/>
            <span style={{color: 'blue', fontStyle: 'italic', paddingLeft: '20px'}}>https://hostname:port/uriprefix/chat/bots/channel_id/messages</span>
            <br/>
            For example:
            <br/>
            <span style={{color: 'blue', fontStyle: 'italic', paddingLeft: '20px'}}>https://bots-samples-dev:8889/ext/apps/chat/bots/channel_id/messages</span>
        </p>
        <p>
            Look for the generated Webhook URL field that has the webhook channel ID as the last part of the URL.  Replace the 'channel_id' in the Outgoing Webhook URL field.
        </p>
    </div>
);

export default content;