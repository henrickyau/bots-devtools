import React from "react";
import PropTypes from 'prop-types';
import Dialog from "material-ui/Dialog";
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn
} from "material-ui/Table";
import FlatButton from "material-ui/FlatButton";

/*
 *  UserListDialog component
 *  Synopsis:
 * 
 *  Props:
 *      Data:
 *        users
 *        open
 *      Functions:
 *        handleClose
 * 
 *  State:
 *      selected
 * 
 *  UI Event Handlers:
 *    handleButtonClose
 *    handleRowSelection
 * 
 *  Lifecycle:
 * 
 *  Requirements on Parent:
 *  1. modal dialog could stay mounted while closed, or could be unmounted on close.  This is controlled by the parent component,
 * 
 *  Notes:
 * 
 *  To Do:
 *  1. Show connected time
 *  2. Allow terminating user
 */

export default class UserListDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        selected: []
    }
  }

  /** UI Handlers */
  handleButtonClose = (event) => {
    this.props.handleClose();
  }

  handleRowSelection = (selectedRows) => {
    this.setState({
      selected: selectedRows
    });
  };

  /** Component Methods */
  isSelected = index => {
    return this.state.selected.indexOf(index) !== -1;
  };

  
  render() {
    const actions = [
      <FlatButton label="Close" primary={false} onClick={this.handleButtonClose} />,
    ];

    const tableRows = this.props.users.map((user, index) => (
      <TableRow selected={this.isSelected(index)} key={index}>
        <TableRowColumn>{user.id}</TableRowColumn>
        <TableRowColumn>{user.origin}</TableRowColumn>
      </TableRow>
    ));

    return (
      <Dialog
        title="Show Connected Users"
        actions={actions}
        modal={true}
        open={this.props.open}
        autoScrollBodyContent={true}
      >
        <div>
          <p>Users recently connected.</p>
          <Table onRowSelection={this.handleRowSelection}>
            <TableHeader
              displaySelectAll={false}
              adjustForCheckbox={false}
              enableSelectAll={false}
            >
              <TableRow>
                <TableHeaderColumn>User ID</TableHeaderColumn>
                <TableHeaderColumn>Origin</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false} deselectOnClickaway={false}>
              {tableRows}
            </TableBody>
          </Table>
        </div>
      </Dialog>
    );
  }
}

UserListDialog.propTypes = {

  users: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    origin: PropTypes.string
  })),
  open: PropTypes.bool,

  handleClose: PropTypes.func.isRequired
}