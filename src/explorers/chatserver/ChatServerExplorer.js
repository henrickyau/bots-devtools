import React from 'react';
import PropTypes from 'prop-types';
import RaisedButton from 'material-ui/RaisedButton';

import ChatServerConfig from './ChatServerConfig';
import ChatServerTester from './ChatServerTester.js'

import _ from 'underscore';

import './chat-server.css';
import '../explorers.css';

/*
 *  ChatServerExplorer component
 *  Synopsis:
 * 
 *  Props:
 *      Data:
 *        windowWidth
 *        windowHeight
 *      Functions:
 * 
 *  State:
 *      chatConfig
 *        chatServerUrl
 *        channelId
 *        conversationProperties
 *        userId
 *      testOpen
 * 
 *  UI Event Handlers:
 *    handleTestClose
 *    handleTestOpen
 * 
 *  Lifecycle:
 *    componentDidUpdate:
 *      persistState
 * 
 *  Requirements on Parent:
 * 
 *  Notes:
 * 
 *  To Do:
 */

export default class ChatServerExplorer extends React.Component {

  constructor(props) {
    super(props);

    this.localStorageIdentifier = 'ChatServerExplorer';
    this.state={
      chatConfig: this.initializeChatConfig(),
      testOpen: false
    };

    this.setChatConfig = this.setChatConfig.bind(this);
    this.resetChatUserId = this.resetChatUserId.bind(this);
  }

  initializeChatConfig() {
    var chatConfig;
    const cachedState = localStorage.getItem(this.localStorageIdentifier);
    if (cachedState) {
        chatConfig = JSON.parse(cachedState);
        chatConfig.chatServerUrl = chatConfig.chatServerUrl || 'http://localhost:8888/ext/apps/chat/bots';
    } else {
        chatConfig = {          
            chatServerUrl: 'http://localhost:8888/ext/apps/chat/bots',
            channelId: ''
        };
    }
    if (!chatConfig.conversationProperties) {
      chatConfig.conversationProperties = {
        "profile": {
          "firstName": 'John',
          "lastName": 'Smith',
          "locale": "en-US",
          "timezoneOffset": 28800000,
          "clientType": 'Web'
        }
      };
    }
    return chatConfig;
  }

  persistState(){
    localStorage.setItem(this.localStorageIdentifier, JSON.stringify(this.state.chatConfig));
  }

  getTesterHeight(windowHeight) {
    var testerInstructionHeight = (this.testerInstruction ? this.testerInstruction.clientHeight : 0);
    return Math.max(windowHeight - testerInstructionHeight, 300);
  }

  componentDidUpdate() {
    this.persistState();
  }

  setChatConfig(chatConfig){
    //this.newChatConfig = Object.assign({}, chatConfig);
    //this.setState({chatConfig: chatConfig});
    this.setState((prevState, props) => ({
      chatConfig: _.extend(prevState.chatConfig, chatConfig)
    }));
    //this.props.returnChatConfigState(chatConfig);
  }

  resetChatUserId() {
    this.setState(function(prevState, props) {
      var newChatConfig = prevState.chatConfig;
      newChatConfig.userId = Date.now();
      return {
        chatConfig: newChatConfig
      };
    });
  }

  //=========

  handleTestOpen = () => {
    this.setState({testOpen: true});
    var self = this;
    var n = 0;
    function scrollTester(){
      if (self.tester){
        self.tester.scrollIntoView();
      } else {
        n++;
        if (n < 5) {
          setTimeout(scrollTester, 200);
        }
      }
    };
    setTimeout(scrollTester, 200);
  };

  handleTestClose = () => {
    this.setState({testOpen: false});
  };



  render() {

    return (

      <div className="explorers">
        <h2>Explore Chat Server</h2>
        <ChatServerConfig chatConfig={this.state.chatConfig} returnChatConfig={this.setChatConfig}/>
        <br/>  
        <div className="ChatServer">       
          <div ref={(testerInstruction) => { this.testerInstruction = testerInstruction; }}>
            <h3>5. Test the bot</h3>
            <p>
            Test chatting with the bot using the chat client below
            </p>
            <div>
              { !this.state.testOpen &&
              <RaisedButton label="Test" 
                onClick={this.handleTestOpen} 
                disabled={!this.state.chatConfig.chatServerUrl || 
                        !this.state.chatConfig.channelId || 
                        !this.state.chatConfig.conversationProperties}/>
              }
              { this.state.testOpen &&
              <RaisedButton label="Close" onClick={this.handleTestClose} />
              }
              <br/>
              <br/>
            </div>
          </div>
          { this.state.testOpen && this.state.chatConfig &&
            <div ref={(tester) => { this.tester = tester; }}>
              <ChatServerTester 
                chatServerUrl={this.state.chatConfig.chatServerUrl}
                channelId={this.state.chatConfig.channelId}
                conversationProperties={this.state.chatConfig.conversationProperties} //??
                targetHeight={this.getTesterHeight(this.props.windowHeight)}
                />
            </div>
          }
        </div>
      </div>


    );
  }
}

ChatServerExplorer.propTypes = {
  windowWidth: PropTypes.number.isRequired,
  windowHeight: PropTypes.number.isRequired
}