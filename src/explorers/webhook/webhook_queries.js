const {webhookUtil} = require("@oracle/bots-node-sdk").Util;

function tryPost(url, callback) {
    let body = {content: "test"};
    const myRequest = new Request(url, {
        method: 'POST',
        headers: {
            "Content-type": "application/json"
        },
        body: JSON.stringify(body)
    });
    fetch(myRequest)
    .then(function(response){
        console.log(response.status);
        if (response.status === 400) {
            callback(null, response.status, response.statusText);
        } else {
            callback('Unexpected Response');
        }
    })
    .catch(function(err){
        console.log(err);
        callback();
    })
  
}

function signAndPostMessage(webhookUrl, secretKey, conversationMessageStr, callback) {
    //return new Promise(function(resolve, reject) {
        const body = conversationMessageStr;
        const buf = Buffer.from(body, 'utf8');

        const headers = {};
        headers['Content-Type'] = 'application/json; charset=utf-8';
        headers['X-Hub-Signature'] = webhookUtil.buildSignatureHeader(buf, secretKey);

        let fetchRequestOptions = {
            method: 'POST',
            headers: headers,
            body: body
        };
        const myRequest = new Request(webhookUrl, fetchRequestOptions);
        let auditHistory = { request: myRequest.clone()};

        fetch(myRequest)
        .then(function(successResponse){
            console.log(successResponse.status);
            auditHistory.response = successResponse.clone();
            callback(null, auditHistory);
        })
        .catch(function(errorResponse){
            if (typeof errorResponse === 'object' && typeof errorResponse.clone === 'function'){
                auditHistory.response = errorResponse.clone();
            } else {
                auditHistory.response = new Response(null, { status: '400', statusText: errorResponse});
            }
            //console.log(errorResponse);
            callback('Unexpected Error', auditHistory);
        });
    //});
}
 
function testBotsWebhookUrl(botsWebhookUrl) {
    return new Promise(function(resolve, reject) {
        let queriesRun = [];
        if (botsWebhookUrl && (botsWebhookUrl.startsWith('http') || botsWebhookUrl.startsWith('https'))) {
            tryPost(botsWebhookUrl, function(err, auditHistory){
                queriesRun.push({
                    resource: "BOTS_WEBHOOK",
                    url: botsWebhookUrl,
                    type: "POST",
                    status: 200
                });
                resolve({
                    status: true,
                    queriesRun: queriesRun
                });
            });            
        } else {
            queriesRun.push({
                resource: "BOTS_WEBHOOK",
                url: botsWebhookUrl,
                type: "POST",
                status: 404
            });
            resolve({
                status: false,
                queriesRun: queriesRun
            });
        }
    });
}

function getWebhookFetchRequest(webhookUrl, secretKey, conversationMessageStr) {
        const body = conversationMessageStr;
        const buf = Buffer.from(body, 'utf8');

        const headers = {};
        headers['Content-Type'] = 'application/json; charset=utf-8';
        headers['X-Hub-Signature'] = webhookUtil.buildSignatureHeader(buf, secretKey);

        let fetchRequestOptions = {
            method: 'POST',
            headers: headers,
            body: body
        };
        return new Request(webhookUrl, fetchRequestOptions);
}
 
function sendWebhookMessage(webhookUrl, secretKey, conversationMessageStr) {
    return new Promise(function(resolve, reject) {

        if (webhookUrl && (webhookUrl.startsWith('http') || webhookUrl.startsWith('https'))) {

            signAndPostMessage(webhookUrl, secretKey, conversationMessageStr, function(err, auditHistory){
                (!err ? resolve : reject)({
                        status: !err,
                        input: {
                            webhookUrl: webhookUrl,
                            secretKey: secretKey,
                            conversationMessageStr: conversationMessageStr
                        },
                        auditHistory: auditHistory
                });
            });            
        } else {
            reject({
                status: false,
                input: {
                    webhookUrl: webhookUrl,
                    secretKey: secretKey,
                    conversationMessageStr: conversationMessageStr
                },
                auditHistory: {
                    error: "Incorrect Input"
                }
            });
        }
    });
}


module.exports = {
    testWebhookUrl: testBotsWebhookUrl,
    testBotsWebhookUrl: testBotsWebhookUrl,
    sendWebhookMessage: sendWebhookMessage,
    getWebhookFetchRequest: getWebhookFetchRequest
};
