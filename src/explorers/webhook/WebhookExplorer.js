import React from 'react';
import PropTypes from 'prop-types';

import WebhookConfig from './WebhookConfig';
import WebhookSender from './WebhookSender';

import _ from 'underscore';

import '../explorers.css';

/*
 *  WebhookExplorer component
 *  Synopsis:
 * 
 *  Props:
 *      Data:
 *        windowWidth
 *        windowHeight
 *      Functions:
 * 
 *  State:
 *      webhookConfig
 *        externalWebhookUrl
 *        secretKey
 *        messagePropertiesStr
 *        userId
 * 
 *  UI Event Handlers:
 * 
 *  Lifecycle:
 *    componentDidUpdate:
 *      persistState
 * 
 *  Requirements on Parent:
 * 
 *  Notes:
 * 
 *  To Do:
 */
export default class WebhookExplorer extends React.Component {

    constructor(props) {
        super(props);
    
        this.localStorageIdentifier = 'WebhookExplorer';
        this.state={
            webhookConfig: this.initializeWebhookConfig(),
            testOpen: false
        };
            
        this.setWebhookConfig = this.setWebhookConfig.bind(this);

    }

    initializeWebhookConfig() {
        var webhookConfig;
        const cachedState = localStorage.getItem(this.localStorageIdentifier);
        if (cachedState) {
            try {
                webhookConfig = JSON.parse(cachedState);
            } catch(err) {
                console.log('webhook config json parse error', err);
            }
        }
        if (!webhookConfig) {
            webhookConfig = {  
                externalWebhookUrl: '',
                secretKey: ''
            };
        }
        return webhookConfig;
    }
    
    persistState(){
        localStorage.setItem(this.localStorageIdentifier, JSON.stringify(this.state.webhookConfig));
    }
    
    setWebhookConfig(webhookConfig){
        //let currentConfig = this.state.webhookConfig;
        this.setState((prevState, props) => ({
            webhookConfig: _.extend(prevState.webhookConfig, webhookConfig)
        }));    
    }
    
    componentDidUpdate() {
        this.persistState();
    }    
    
    render() {

        return (
            <div className="explorers">
                <h2>Explore Webhook Integration</h2>
                <div className="explorers-indent">
                    <WebhookConfig 
                        webhookConfig={this.state.webhookConfig}
                        returnWebhookConfig={this.setWebhookConfig}
                    />
                </div>
                <br/>
                <div className="explorers-indent">
                    <h3>3. Mock the Bot Response</h3>
                    <p>
                        Craft a message as a bot response to send to the Webhook Channel.  Put in the User Id to match the bot session.
                    </p>
                    <WebhookSender 
                        webhookConfig={this.state.webhookConfig}
                    />
                </div>
            </div>
        );
    }

}

WebhookExplorer.propTypes = {
    windowWidth: PropTypes.number.isRequired,
    windowHeight: PropTypes.number.isRequired
}