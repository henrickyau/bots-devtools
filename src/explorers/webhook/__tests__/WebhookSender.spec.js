import React from 'react';
import {render, Simulate, wait, renderIntoDocument, cleanup, fireEvent, prettyDOM} from 'react-testing-library';
import 'dom-testing-library/extend-expect';
import { shallow } from 'enzyme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import WebhookSender from '../WebhookSender';
import messageTemplates from '../WebhookSender/message_templates.js';

import conversions from "webidl-conversions";

const webhookCfg = {
    externalWebhookUrl: 'http://sc12ano.us.oracle.com:8888/ext/apps/chat/bots/F923E838-86D6-4088-9EBE-632B5F889DF9/messages',
    secretKey: 'h2JKVRHUvFkQTMyQWEBTptYO3n48eRzn'
};
const userIdLabel = 'User Id';
const myUserId = 'User45678';
let Rendered;
const componentMarkup = 
    <MuiThemeProvider>
        <WebhookSender webhookConfig={webhookCfg}/>
    </MuiThemeProvider>;
const fetchRejectMsg = 'TypeError: Failed to fetch'; 
const fetchSuccessMsg = 'I am good'; 

describe("WebhookSender", () => {

    beforeEach(() => {
        Rendered = render(componentMarkup);
    });

    it("should render default WebhookSender", () => {
        expect(Rendered.getByLabelText(userIdLabel)).toBeInTheDOM();
    });

    it("should match snapshot", () => {
        const wrapper = shallow(componentMarkup);
        expect(wrapper.getElements()).toMatchSnapshot();
    });

    it("should support userid update", () => {
        const textInputNode = Rendered.getByLabelText(userIdLabel);
        textInputNode.value = myUserId;
        Simulate.change(textInputNode);
        expect(Rendered.getByLabelText(userIdLabel)).toHaveAttribute('value', myUserId);
    });

});

describe("WebhookSender Advanced", () => {

    beforeEach(() => {
        Rendered = renderIntoDocument(componentMarkup);
    });

    afterEach(cleanup);

    it("should support template selection", () => {
        const attachementBtnNode = Rendered.getByText('Attachment');
        fireEvent(attachementBtnNode, new MouseEvent('click', {
            bubbles: true, // click events must bubble for React to see it
            cancelable: true,
          }));
        expect(Rendered.getByLabelText('Conversation Message Payload')).toHaveTextContent(JSON.stringify(messageTemplates.templateAttachment, null, 4));
    });

});


describe("WebhookSender Fetch Fail", () => {

    /*beforeAll(() => {
        if (!HTMLElement.prototype.scrollIntoView) {
          HTMLElement.prototype.scrollIntoView = () => {}
        }
    })*/

    beforeEach(() => {
        Rendered = renderIntoDocument(componentMarkup);
    });

    afterEach(() => {
        cleanup();
        fetch.resetMocks();
    });

    it("should show render results", async (done) => {
        const attachementBtnNode = Rendered.getByText('Attachment');
        fireEvent(attachementBtnNode, new MouseEvent('click', {
            bubbles: true, // click events must bubble for React to see it
            cancelable: true,
          }));

        //fetch.mockResponseOnce(JSON.stringify({ data: '12345', status: '200' }));
        fetch.mockReject(fetchRejectMsg);
        const sendBtnNode = Rendered.getByText('Send');
        fireEvent(sendBtnNode, new MouseEvent('click', {
            bubbles: true, // click events must bubble for React to see it
            cancelable: true,
          }));
        //console.log(prettyDOM(Rendered.container));

        await wait(() => Rendered.getByText(fetchRejectMsg));
        expect(Rendered.getByText(fetchRejectMsg, {exact: false})).toBeInTheDOM();
        done();
    });

});


describe("WebhookSender Fetch Success", () => {

    beforeEach(() => {
        Rendered = renderIntoDocument(componentMarkup);
    });

    afterEach(() => {
        cleanup();
        fetch.resetMocks();
    });

    xit("should show render results", async (done) => {
        const attachementBtnNode = Rendered.getByText('Attachment');
        fireEvent(attachementBtnNode, new MouseEvent('click', {
            bubbles: true, // click events must bubble for React to see it
            cancelable: true,
          }));

        var blob = new Blob([fetchSuccessMsg]);
        //fetch.mockResponseOnce(new Response(null, {status: 200, statusText: 'OK'}));
        fetch.mockResponseOnce(new Response());
        //fetch.mockReject(fetchRejectMsg);
        const sendBtnNode = Rendered.getByText('Send');
        fireEvent(sendBtnNode, new MouseEvent('click', {
            bubbles: true, // click events must bubble for React to see it
            cancelable: true,
          }));

        await wait(() => Rendered.getByText(fetchSucessMsg));
        expect(Rendered.getByText(fetchRejectMsg, {exact: false})).toBeInTheDOM();
        done();
    });

});
