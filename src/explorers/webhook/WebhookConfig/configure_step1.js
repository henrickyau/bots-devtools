import React from 'react';

var content = (
    <div>
        <h3>1. Set up your own Webhook integration server, or use a sample like Chat Server or Alexa.</h3>
        <p>
            A webhook endpoint is an HTTPS endpoint used to receive messages from a bot
        </p>
        <br/>
        For Chat Server Sample: 
        <p className="explorers-indent">
                The URI is in the form:
                <br/>
                <span style={{color: 'blue', fontStyle: 'italic', paddingLeft: '20px'}}>https://hostname:port/[uriprefix]/chat/bots/[channel_id]/messages</span>
                <br/>
        </p>
        <br/> 
        For Alexa Sample: 
        <p className="explorers-indent">
                The URI is in the form:
                <br/>
                <span style={{color: 'blue', fontStyle: 'italic', paddingLeft: '20px'}}>https://hostname:port/[uriprefix]/alexa-singleBot/singleBotWebhook/messages</span>
                <br/>
        </p>
    </div>
);

export default content;