import React from 'react';
import PropTypes from 'prop-types';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

import StatusSuccess from 'material-ui/svg-icons/action/check-circle';
import StatusFailed from 'material-ui/svg-icons/action/highlight-off';
import {red500, green500} from 'material-ui/styles/colors';

import webhookQueries from '../webhook_queries.js';

import configure_step1 from './configure_step1';
import '../../explorers.css';
import _ from 'underscore';

/*
 *  WebhookConfig component
 *  Props:
 *    Data:
 *      webhookConfig
 *    Functions:
 *      returnWebhookConfig(webhookConfig)
 * 
 *  State:
 *        externalWebhookUrl
 *        secretKey
 *
 *  UI Event Handlers:
 * 
 *  Lifecycle:
 */
export default class WebhookConfig extends React.Component {

    constructor(props) {
        super(props);

        this.webhookConfigProperties = [
            'externalWebhookUrl',
            'secretKey'
        ];

        /*this.defaultState = _.reduce(this.webhookConfigProperties, function(memo, property){
            memo[property] = "";
            return memo;
        }, {});
        this.passedProps = _.defaults(_.pick(this.props.webhookChannelConfig,this.webhookConfigProperties), this.defaultState);

        this.state = Object.assign({}, this.passedProps, {
                    externalWebhookStatus: 0,
                    externalWebhookStatusText: ''
                });*/

        this.state = Object.assign({}, this.props.webhookConfig, {
            externalWebhookStatus: 0,
            externalWebhookStatusText: ''
        });
        console.log(this.state);
    }

    /** UI Event Handlers */
    handleButtonWebhookCheck = () => {
        let self = this;
        if (this.state.externalWebhookUrl){
            webhookQueries.testWebhookUrl(this.state.externalWebhookUrl)
            .then(function(result){
                self.setState({
                    externalWebhookStatus: 1,
                    externalWebhookStatusText: "success"
                }, function(){
                    self.returnWebhookConfig();
                });
            })
            .catch(function(err){
                self.setState({
                    externalWebhookStatus: -1,
                    externalWebhookStatusText: "failure"
                }, function(){
                    self.returnWebhookConfig();
                });
            });    
        }
    };

    handleChangeField = (event) => {
        let state = {};
        state[event.target.name] = event.target.value;
        this.setState(state);
    }

    returnWebhookConfig() {
        let config = _.pick(this.state, this.webhookConfigProperties);
        //console.log(this.state);
        console.log('webhookConfig', config);
        this.props.returnWebhookConfig(config);
      }
     
    render(){

        var externalWebhookStatus;
        switch(this.state.externalWebhookStatus) {
            case 1:     
                externalWebhookStatus = <span><StatusSuccess color={green500} viewBox="-5 -5 36 36" style={{width: '36px', height: '36px', verticalAlign: 'middle'}}/><span style={{color: 'rgba(0, 0, 0, 0.3)'}}>{this.state.externalWebhookStatusText}</span></span>;
                break;
            case -1:
                externalWebhookStatus = <span><StatusFailed color={red500} viewBox="-5 -5 36 36" style={{width: '36px', height: '36px', verticalAlign: 'middle'}}/><span style={{color: 'rgba(0, 0, 0, 0.3)'}}>{this.state.externalWebhookStatusText}</span></span>;
                break;
            default:
                externalWebhookStatus = <span/>
        };

        return(
            <div>
                {configure_step1}
                <br/>  
                <div>
                    <h3>2. Put in the Webhook configuration</h3>
                    <p>
                        Put in the webhook url on the Bots side as well as on the external side, and also the secret key.  They are found in the Bots UI.
                    </p>
                    <p>
                        Please ensure that the URLs are resolvable from the browser.
                    </p>
                    <div>
                        <TextField name="externalWebhookUrl" floatingLabelText="External Webhook URL" fullWidth={true} value={this.state.externalWebhookUrl} onChange={this.handleChangeField}/>
                        <TextField name="secretKey" floatingLabelText="Secret Key" fullWidth={true} value={this.state.secretKey} onChange={this.handleChangeField}/>
                        <RaisedButton label="Apply" onClick={this.handleButtonWebhookCheck} style={{verticalAlign: 'middle'}}/>
                        &nbsp;
                        { this.state.externalWebhookUrl && externalWebhookStatus}
                    </div>
                    {/*<ChannelConfig 
                        webhookChannelConfig={this.state}
                        returnChannelConfig={this.setChannelConfig}
                    />*/}
                </div>     
                <br/>  
            </div>
        );
    }
}

WebhookConfig.propTypes = {

    webhookConfig: PropTypes.shape({
      externalWebhookUrl: PropTypes.string,
      secretKey: PropTypes.string
    }),
  
    returnWebhookConfig: PropTypes.func.isRequired
}