var message_templates = {
    templateText: require('./templates/text.json'),
    templateTextActions: require('./templates/text_action.json'),
    templateAttachment: require('./templates/attachment.json'),
    templateCard: require('./templates/card.json'),
    templateRaw: require('./templates/raw.json'),
    templateLocation: require('./templates/location.json')
};

module.exports = message_templates;