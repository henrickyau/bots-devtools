import React, { Component } from 'react';
import PropTypes from 'prop-types';

import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import CircularProgress from 'material-ui/CircularProgress';

import JSONProperties from '../../common/JSONProperties.js'
import FetchRequest from '../../common/FetchRequest'
import FetchResponse from '../../common/FetchResponse'
import webhookQueries from '../webhook_queries.js';
import messageTemplates from './message_templates.js';

import '../../explorers.css';

import _ from 'underscore';

export default class WebhookSender extends Component {
    constructor(props) {
        super(props);

        this.defaultUserId = "12345"

        this.state = {
            contentMessage: {},
            userId: '',
            fetchRequest: null,
            fetchResponse: null
        };

        this.setContentMessage = this.setContentMessage.bind(this);
    }

    setResponseState(fetchResponse) {
        this.setState({
            fetchResponse: fetchResponse.clone(),
            fetchInProgress: false
        });
        console.log(fetchResponse.status);
    }

    doFetch(webhookFetchRequest) {
        const self = this;
        this.setState({
            fetchRequest: webhookFetchRequest.clone(), // clone request for display as body can only be read once
            fetchInProgress: true,
            showSpinner: true
        });
        setTimeout(function(){
            self.setState({showSpinner: false});
        }, 2000);

        fetch(webhookFetchRequest)
                .then(function(successResponse){
                    self.setResponseState(successResponse);
                })
                .catch(function(errorResponse){
                    self.setResponseState(
                        (typeof errorResponse === 'object' && typeof errorResponse.clone === 'function'
                            ? errorResponse.clone()
                            : new Response(null, { status: '400', statusText: errorResponse}))
                    );
            
                });
    }

    /** UI Event Handlers */
    handleButtonSend = () => {
        let self = this;
        if (this.props.webhookConfig && this.props.webhookConfig.externalWebhookUrl && this.props.webhookConfig.secretKey){

            let webhookFetchRequest = webhookQueries.getWebhookFetchRequest(
                this.props.webhookConfig.externalWebhookUrl,
                this.props.webhookConfig.secretKey,
                JSON.stringify(this.state.contentMessage));

            this.doFetch(webhookFetchRequest);
        }
    };
    
    handleChangeUserId = (event) => {
        this.setState({userId: event.target.value});
        let newUserId = (event.target.value ? event.target.value : this.defaultUserId);
        if (this.state.contentMessage && this.state.contentMessage.messagePayload) {
            let newMessage = this.state.contentMessage;
            newMessage.userId = newUserId;
            this.setState({contentMessage: newMessage});
        }
    }
    
    handleUseTemplate = (event) => {
        let userId = (this.state.userId ? this.state.userId : this.defaultUserId);
        let template = _.clone(messageTemplates[event.currentTarget.name]);
        if (template){
            template.userId = userId + "";
            this.setState({contentMessage: template});    
        }
    }

    setContentMessage(properties){
        //if (properties) {
            this.setState({
                contentMessage: properties
            });
        //}
    }

    componentDidUpdate(){
        if (this.state.fetchInProgress || this.state.showSpinner){
            if (this.fetchBottom){
                this.fetchBottom.scrollIntoView();
            }    
        }
    }

    render() {

        return (
            <div>
                <TextField
                    fullWidth={true}
                    floatingLabelText="User Id"
                    name="userId"
                    value={this.state.userId}
                    onChange={this.handleChangeUserId}
                />

                <div>
                <p> Use the following templates to start crafting the message response from bot. </p>
                <FlatButton name="templateText" label="Text" primary={true} onClick={this.handleUseTemplate}/>
                <FlatButton name="templateTextActions" label="Text + Actions" secondary={true} onClick={this.handleUseTemplate}/>
                <FlatButton name="templateAttachment" label="Attachment" primary={true} onClick={this.handleUseTemplate}/>
                <FlatButton name="templateCard" label="Card" secondary={true} onClick={this.handleUseTemplate}/>
                <FlatButton name="templateLocation" label="Location" primary={true} onClick={this.handleUseTemplate}/>
                <FlatButton name="templateRaw" label="Raw" secondary={true} onClick={this.handleUseTemplate}/>
                </div>
                <JSONProperties 
                    label={"Conversation Message Payload"}
                    properties={this.state.contentMessage}
                    returnProperties={this.setContentMessage}
                    maxRows={20}
                />
                <RaisedButton label="Send" onClick={this.handleButtonSend} style={{verticalAlign: 'middle'}}/>
                <br/>
                { this.state.fetchRequest &&
                    <div>
                        <div className='explorers-2-equal-column-left'>
                            <FetchRequest 
                                fetchRequest={this.state.fetchRequest} 
                            />
                        </div>
                        <div className='explorers-2-equal-column-right'>
                            { this.state.fetchInProgress || this.state.showSpinner &&
                                <CircularProgress size={60} thickness={5} />
                            }
                            { !(this.state.fetchInProgress || this.state.showSpinner) &&
                                <FetchResponse 
                                    fetchResponse={this.state.fetchResponse} 
                                />
                            }
                        </div>
                        <div ref={(bottom) => { this.fetchBottom = bottom; }}>
                            <p>&nbsp;</p>
                        </div>
                    </div>
                }
            </div>
        );
    }



}

WebhookSender.propTypes = {

    webhookConfig: PropTypes.shape({
      externalWebhookUrl: PropTypes.string,
      secretKey: PropTypes.string
    }).isRequired
    
}
  