import React from 'react';
import Dialog from 'material-ui/Dialog';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import NavigationClose from 'material-ui/svg-icons/navigation/close';

export default class WebViewDialog extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
        //chatServerUrl: "",
    };
}

  render() {

    return (

        <Dialog
          modal={true}
          open={this.props.open}
        >
            <div>
            <AppBar iconElementRight={<IconButton onClick={this.props.handleClose}><NavigationClose /></IconButton>}/>;
                <iframe id="WebView"
                    width={this.props.viewWidth}
                    frameBorder="0"
                    height={this.props.viewHeight}
                    src={this.props.viewUrl}>
                </iframe>
            </div>
        </Dialog>
    );
  }
}
