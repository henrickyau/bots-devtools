function openUrl(urlAction){
    if (urlAction.type === 'url' && urlAction.url) {
        var win = window.open(urlAction.url, '_blank');
        win.focus();
    }
}

module.exports = {
    openUrl: openUrl
};
