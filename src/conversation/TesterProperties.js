import React, { Component } from 'react';
import TextField from 'material-ui/TextField';

export default class TesterProperties extends Component {

  render() {
    var rendering;
    var messageProperties;
    var summaryProperties;
    var conversationBoxStyle = {
      overflowY: 'auto',
      height: this.props.targetHeight,
    };

    if (this.props.testerProperties) {
      if (this.props.testerProperties.conversationMessagePayload) {
        messageProperties = this.props.testerProperties;
        var messageDate = new Date(messageProperties.messageInfo.timestamp);
        var dateOptions = {weekday: 'short', hour: 'numeric', minute: 'numeric', second: 'numeric', timeZoneName: 'short'};
        rendering = (
          <div style={conversationBoxStyle}>
            <TextField
              multiLine={false}
              value={messageDate ? messageDate.toLocaleDateString(navigator.language, dateOptions) + ' ('+messageProperties.messageInfo.responseTime+'ms)' : ''}
              floatingLabelText="Timestamp (Response Time)"
              style={{fontSize: '13px', lineHeight: 1}}
              fullWidth={true}
            /><br />
            <TextField
              multiLine={true}
              value={JSON.stringify(messageProperties.transportMessagePayload ? messageProperties.transportMessagePayload : messageProperties.conversationMessagePayload, null, 2)}
              floatingLabelText="Chat Message Payload"
              style={{fontSize: '13px', lineHeight: 1}}
              fullWidth={true}
            /><br />
          </div>
        );
      } else {
        summaryProperties = this.props.testerProperties;
        rendering = (
          <div style={conversationBoxStyle}>
            <TextField
              multiLine={true}
              value={JSON.stringify(summaryProperties.wsInfo, null, 2)}
              floatingLabelText="Websocket Connection"
              style={{fontSize: '13px', lineHeight: 1}}
              fullWidth={true}
            /><br />
          </div>
        );
      }
    } else {
      rendering = (<div>None</div>);
    }

    return rendering;
  }

}

