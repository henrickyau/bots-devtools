import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {List, ListItem} from 'material-ui/List';
import Paper from 'material-ui/Paper';
import IconButton from 'material-ui/IconButton';
import MoreIcon from 'material-ui/svg-icons/navigation/more-horiz';
import {grey400, darkBlack, lightBlack} from 'material-ui/styles/colors';

import './ResponseItem.css';

import TextMsg from './messages/TextMsg'
import CardsMsg from './messages/CardsMsg'
import AttachmentMsg from './messages/AttachmentMsg'
import LocationMsg from './messages/LocationMsg'
import GlobalActions from './messages/GlobalActions'


const flexContainer ={
  display: 'flow-root'
};

const paperStyleBot = {
  float: 'left',
  marginLeft: 10,
  marginRight: 30,
  padding: 3,
  textAlign: 'center',
  fontSize: '14px'
  //display: 'inline-block',
};

const paperStyleSelf = {
  float: 'right',
  marginLeft: 30,
  marginRight: 10,
  padding: 5,
  textAlign: 'center',
  //boxShadow: '#00cefdbf 0px 10px 30px, rgba(0, 0, 0, 0.28) 0px 6px 10p',
  //boxShadow: ['#00cefdbf 0px 10px 30px', 'rgba(0, 0, 0, 0.28) 0px 6px 10p'].join(),
  color: '#0083ea',
  fontSize: '14px'
  //display: 'inline-block',
};


class ResponseItem extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const partyStyle = (this.props.chatEnvelopeMessage.messageInfo.fromType === 'USER' ? paperStyleSelf : paperStyleBot);
    var self = this;
    let rendering;
    let bubbleContent;
    let globalActions;
    var conversationMessage = (this.props.chatEnvelopeMessage ? this.props.chatEnvelopeMessage.conversationMessagePayload : null);
    if (!conversationMessage){
      rendering = <div/>;
    } else {
        if (conversationMessage.type === 'text' || conversationMessage.type === 'postback') {
          rendering = <TextMsg
                        messagePayload={conversationMessage}
                        selectAction={this.props.selectAction}
                      />;                     
        } else if (conversationMessage.type === 'card') {
          rendering = <CardsMsg
            messagePayload={conversationMessage}
            selectAction={this.props.selectAction}
          />;      
        } else if (conversationMessage.type === 'attachment') {
          rendering = <AttachmentMsg
            messagePayload={conversationMessage}
            selectAction={this.props.selectAction}
          />;      
        } else if (conversationMessage.type === 'location') {
          rendering = <LocationMsg
            messagePayload={conversationMessage}
            selectAction={this.props.selectAction}
          />;      
        }

        if (conversationMessage.globalActions) {
          globalActions = 
                  <GlobalActions 
                      globalActions={conversationMessage.globalActions}
                      selectAction={this.props.selectAction}
                  />
        }
    }
    const iconButtonElement = ( this.props.selectItem ?
      <IconButton
        touch={true}
        onClick={this.props.selectItem.bind(this,this.props.itemIndex)}
        style={{top: '5px'}}
      >
        {this.props.selectItem &&
           <MoreIcon color={(this.props.selectedItemIndex===this.props.itemIndex ? darkBlack : grey400)}/>}
      </IconButton>
      : null
    );
    
    bubbleContent = 
      <div style={flexContainer}>
        <Paper style={partyStyle} zDepth={3}>
          {rendering}
        </Paper>
        <div style={{clear:'both', padding: '10px 10px 0px 10px'}}>
          {globalActions}
        </div>
      </div>;

   return ( this.props.selectItem ? 
    <ListItem rightIconButton={iconButtonElement}>
      {bubbleContent}
    </ListItem> 
    :
    <ListItem >
      {bubbleContent}
    </ListItem>
    );
  }
}

export default ResponseItem;

ResponseItem.propTypes = {

  chatEnvelopeMessage: PropTypes.object.isRequired,
  itemIndex: PropTypes.number.isRequired,
  selectedItemIndex: PropTypes.number,

  selectAction: PropTypes.func,
  selectItem: PropTypes.func
}