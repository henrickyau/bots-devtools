import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {List, ListItem} from 'material-ui/List';

export default class Actions extends Component {

    
    render() {
        let self = this;
        return (
            <List style={{}}>
                {this.props.actions.map(function(action, index){
                    return (
                        <ListItem key={index} style={{fontSize: '14px', lineHeight: '8px'}} onClick={self.props.selectAction.bind(null, action)}>
                            {action.label}
                        </ListItem>
                    );
                })}
            </List>
        );
    }

}
