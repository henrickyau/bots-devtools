import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Divider from 'material-ui/Divider';
import RightArrowIcon from 'material-ui/svg-icons/navigation/chevron-right';
import LeftArrowIcon from 'material-ui/svg-icons/navigation/chevron-left';
import IconButton from 'material-ui/IconButton';
import {grey400, darkBlack, lightBlack} from 'material-ui/styles/colors';

import CardLayout from './CardLayout';
import Actions from './Actions'

export default class CardsMsg extends Component {

    constructor(props) {
        super(props);

        this.state = {
            currentCard: 0
        };
    }

    nextCard = () => {
        if (this.state.currentCard < this.props.messagePayload.cards.length-1) {
            this.setState((prevState, props) => ({
                currentCard: prevState.currentCard+1
            }));
        }
    }

    prevCard = () => {
        if (this.state.currentCard > 0) {
            this.setState((prevState, props) => ({
                currentCard: prevState.currentCard-1
            }));
        }
    }

    render() {
        let self = this;
        let cards = <div/>;
        let actions = <div/>;
        if (this.props.messagePayload.type === 'card') {
            if (this.props.messagePayload.layout === 'vertical' || this.props.messagePayload.cards.length <= 1) {
                cards = 
                    <div>
                        {this.props.messagePayload.cards.map((card, index) =>
                            <CardLayout key={index} card={card} selectAction={self.props.selectAction}/>
                        )}
                    </div>;
            } else {
                const prevElementDisabled = (this.state.currentCard === 0);
                const prevElement = 
                    <IconButton
                        touch={true}
                        onClick={this.prevCard}
                        disabled={prevElementDisabled}
                        style={{float: 'left'}}>
                            <LeftArrowIcon color={ (prevElementDisabled ? grey400 : darkBlack)}/>}
                    </IconButton>;
                const nextElementDisabled = (this.state.currentCard >= this.props.messagePayload.cards.length);
                const nextElement = 
                    <IconButton
                        touch={true}
                        onClick={this.nextCard}
                        disabled={nextElementDisabled}
                        style={{float: 'right'}}>
                            <RightArrowIcon color={ nextElementDisabled ? grey400: darkBlack}/>}
                    </IconButton>;
                cards = 
                    <div style={{display: 'flex', alignItems: 'center'}}>
                        {prevElement}
                        <CardLayout 
                            key={this.state.currentCard} 
                            card={this.props.messagePayload.cards[this.state.currentCard]} 
                            selectAction={self.props.selectAction}/>
                        {nextElement}
                    </div>
            }
            if (this.props.messagePayload.actions) {
                actions = 
                    <div>
                        <Divider />
                        <Actions 
                            actions={this.props.messagePayload.actions}
                            selectAction={this.props.selectAction}
                        />
                    </div>;
            }

        }
    
        return (
            <div>
                {cards}
                {actions}
            </div>
        );
    }

}
