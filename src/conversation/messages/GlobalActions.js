import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {blue300, white} from 'material-ui/styles/colors';

import Chip from 'material-ui/Chip';

export default class GlobalActions extends Component {

    constructor(props) {
        super(props);
        this.styles = {
            chip: {
                margin: 4,
                backgroundColor: white,
                borderColor: blue300
            },
            wrapper: {
                display: 'flex',
                flexWrap: 'wrap',
            }
        };
    }
    
    render() {
        let self = this;
        return (
            <div style={self.styles.wrapper} >
                {this.props.globalActions.map(function(action, index){
                    return (
                        <Chip key={index} style={self.styles.chip} labelColor={blue300} onClick={self.props.selectAction.bind(null, action)}>
                            {action.label}
                        </Chip>
                    );
                })}
            </div>
        );
    }

}
