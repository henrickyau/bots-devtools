import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Divider from 'material-ui/Divider';

import Actions from './Actions'

export default class AttachmentMsg extends Component {

    render() {

        return (
        <div>
            <div>
                { this.props.messagePayload.attachment.type === 'image' &&
                    <img 
                        style={{height: '100%', width: '100%', objectFit: 'contain'}} 
                        src={this.props.messagePayload.attachment.url}
                    />      
                }
                { this.props.messagePayload.attachment.type === 'video' &&
                    <video 
                        style={{height: '100%', width: '100%', objectFit: 'contain'}} 
                        autoPlay 
                        controls 
                        src={this.props.messagePayload.attachment.url}
                    />
                }
                { this.props.messagePayload.attachment.type === 'audio' &&
                    <audio 
                        controls 
                        src={this.props.messagePayload.attachment.url}>
                            Your browser does not support the <code>audio</code> element.
                    </audio>
                }                
                { this.props.messagePayload.attachment.type === 'file' &&
                    <a 
                        href={this.props.messagePayload.attachment.url}
                        target="_blank"
                        style={{wordBreak: 'break-all'}}>
                        {this.props.messagePayload.attachment.url}
                    </a>
                }
            </div>
            {this.props.messagePayload.actions &&
                <div>
                    <Divider />
                    <Actions 
                        actions={this.props.messagePayload.actions}
                        selectAction={this.props.selectAction}
                    />
                </div>
            }
        </div>
        );
    }

}
