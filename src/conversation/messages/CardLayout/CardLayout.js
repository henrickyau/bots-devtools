import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';

export default class CardLayout extends Component {

    render() {
        let self = this;
        let cardUrl = (this.props.card.url ?
            <CardText>
                <a href={this.props.card.url} target="_blank">{this.props.card.title}</a>
            </CardText>
        : <div/>);
        let cardTitle = 
            <CardTitle 
                title={this.props.card.title} 
                subtitle={this.props.card.description} >
            </CardTitle>;

        return (
            <Card containerStyle={{padding: '10px', margin: '10px', border: '1px'}}>

                { !this.props.card.imageUrl &&
                    cardTitle
                }
                { this.props.card.imageUrl &&
                    <CardMedia
                        overlay={cardTitle}>
                        <img src={this.props.card.imageUrl} alt="" />
                    </CardMedia>
                }
                { this.props.card.url &&
                    cardUrl
                }
                { this.props.card.actions &&
                    <CardActions>
                        {this.props.card.actions.map(function(action, index){
                            return (
                                <RaisedButton key={index} label={action.label} onClick={self.props.selectAction.bind(null, action)}/>
                            );
                        })}
                    </CardActions>
                }
            </Card>
        );
    }

}
