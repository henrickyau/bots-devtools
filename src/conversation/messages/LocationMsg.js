import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Divider from 'material-ui/Divider';
import {List, ListItem} from 'material-ui/List';

import Actions from './Actions'

const Joi = require('joi-browser');
//const MessageModel = require('../../common/MessageModel.js')(Joi);
//const MessageModel = require('../common/MessageModel.js')(Joi);
const MessageModel = require('@oracle/bots-node-sdk').MessageModel;

export default class LocationMsg extends Component {

    render() {
        let msgPayload = this.props.messagePayload;
        let mapAction = (msgPayload.location.url ? 
                            MessageModel.urlActionObject('Open Map', null, msgPayload.location.url) :
                            null);
        return (
            <div>
                { msgPayload.location.title &&
                    <div style={{padding: '3px'}}>
                        {msgPayload.location.title}
                    </div>
                }
                { msgPayload.location.url &&
                    <List style={{}}>
                        <ListItem key={0} style={{fontSize: '14px', lineHeight: '8px'}} onClick={this.props.selectAction.bind(null, mapAction)}>
                            {mapAction.label}
                        </ListItem>
                    </List>
                }
                { msgPayload.actions &&
                    <div>
                        <Divider />
                        <Actions 
                            actions={msgPayload.actions}
                            selectAction={this.props.selectAction}
                        />
                    </div>
                }
            </div>
        );
    }

}
