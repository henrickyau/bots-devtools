import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Divider from 'material-ui/Divider';

import Actions from './Actions'

export default class TextMsg extends Component {

    render() {
    
        return (
            <div>
                { this.props.messagePayload.text &&
                    <div style={{padding: '3px'}}>
                        {this.props.messagePayload.text}
                    </div>
                }
                { this.props.messagePayload.actions &&
                    <div>
                        <Divider />
                        <Actions 
                            actions={this.props.messagePayload.actions}
                            selectAction={this.props.selectAction}
                        />
                    </div>
                }
            </div>
        );
    }

}
