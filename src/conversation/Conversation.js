import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {List, ListItem} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import ResponseItem from './ResponseItem';

import _ from 'underscore';

/*
 *  Conversation component
 *  Synopsis:
 * 
 *  Props:
 *      Data:
 *         conversation
 *         selectedItemIndex
 *      Functions:
 *         selectAction
 *         setEndMessage
 *         selectItem
 * 
 *  State:
 * 
 *  UI Event Handlers:
 * 
 *  Lifecycle:
 * 
 *  Requirements on Parent:
 * 
 *  Notes:
 * 
 *  To Do:
 */
export default class Conversation extends Component {

  render() {
    var self = this;
    const numItems = this.props.conversation.length;
    const responseItems = _.filter(this.props.conversation, function(item) {
      return (item.conversationMessagePayload && item.conversationMessagePayload && ['text', 'card', 'attachment', 'location'].indexOf(item.conversationMessagePayload.type) >= 0);
    }).map((chatEnvelopeMessage, index) =>
      <div ref={(endElement) => { if (index === numItems -1) self.props.setEndMessage(endElement); }} key={index} value={index}>
        <ResponseItem 
          chatEnvelopeMessage={chatEnvelopeMessage} 
          itemIndex={index}
          selectedItemIndex={this.props.selectedItemIndex}
          selectAction={this.props.selectAction}
          selectItem={this.props.selectItem}
        />
      </div>
    );
    return(
      <div style={{border: '1px'}}>
        <Divider />
        <List>
            {responseItems}
        </List>
      </div>
    );
  }

}

Conversation.propTypes = {

  selectedItemIndex: PropTypes.number,
  conversation: PropTypes.arrayOf(PropTypes.object),

  selectAction: PropTypes.func,
  setEndMessage: PropTypes.func,
  selectItem: PropTypes.func
}
