//const Joi = require('joi-browser');
//const MessageModel = require('../common/MessageModel.js')(Joi);
//const sdk = require('@oracle/bots-node-sdk');
//const MessageModel = sdk.Lib.MessageModelBrowser;
const MessageModel = require('@oracle/bots-node-sdk').MessageModel;

export default class WsMessageModel {

    static toBotMessage(conversationMessagePayload, botId, properties) {

        let message = {
            to: {
                type: 'bot',
                id: botId
            },
            messagePayload: new MessageModel(conversationMessagePayload).messagePayload()
        };
        if (properties) {
            message = Object.assign(message, properties);
        }
        return message;

    }

}
