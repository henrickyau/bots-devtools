//const Joi = require('joi-browser');
const MessageModel = require('@oracle/bots-node-sdk').MessageModel;

export default class ChatEnvelopeMessageModel {

    static chatEnvelopeMessage(conversationMessagePayload, transportMessagePayload, messageInfo) {

        let message = {
            conversationMessagePayload: new MessageModel(conversationMessagePayload).messagePayload(),
            transportMessagePayload: JSON.parse(JSON.stringify(transportMessagePayload)),
            messageInfo: ChatEnvelopeMessageModel._messageInfo(messageInfo)
        }
        if (message.conversationMessagePayload && message.messageInfo) {
            return message;
        } else {
            return null;
        }

    }

    static _messageInfo(messageInfo) {
        let info = {
            timestamp: (messageInfo.timestamp && typeof messageInfo.timestamp === 'number' ? messageInfo.timestamp : Date.now()),
            responseTime: (typeof messageInfo.responseTime === 'number' ? messageInfo.responseTime : null),
            fromType: (messageInfo.fromType && ['USER','BOT','AGENT'].indexOf(messageInfo.fromType) > -1 ? messageInfo.fromType : null),
            fromId: messageInfo.fromId,
            channelType: messageInfo.channelType,
            channelId: messageInfo.channelId
        };
        if (info.fromType) {
            return info;
        } else {
            return null;
        }
    }

}

