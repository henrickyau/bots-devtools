var message_templates = {
    call: require('./templates/call_action.json'),
    location: require('./templates/location_action.json'),
    postback: require('./templates/postback_action.json'),
    url: require('./templates/url_action.json')
};

module.exports = message_templates;