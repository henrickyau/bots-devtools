import React, { Component } from 'react';
import PropTypes from 'prop-types';

import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';

import JSONProperties from '../../common/JSONProperties.js'
import messageTemplates from './message_templates.js';

//import '../../explorers.css';

import _ from 'underscore';

export default class ActionComposer extends Component {
    constructor(props) {
        super(props);

        //this.defaultContentMessageStr = JSON.stringify({}, null, 4);
        this.defaultUserId = "12345"

        this.state = {
            contentMessage: {},
            userId: ''
        };

        this.setContentMessage = this.setContentMessage.bind(this);
    }

    initComponent() {

    }

    /** UI Event Handlers */
    handleChangeField = (event) => {
        let state = {};
        state[event.target.name] = event.target.value;
        this.setState(state);
    }
    
    handleUseTemplate = (event) => {
        let userId = (this.state.userId ? this.state.userId : this.defaultUserId);
        let template = _.clone(messageTemplates[event.currentTarget.name]);
        if (template){
            template.userId = userId + "";
            this.setState({contentMessage: template});    
        }
    }

    setContentMessage(properties){
        //if (properties) {
            this.setState({
                contentMessage: properties
            });
        //}
    }

    render() {

        return (
            <div>
                <div>
                <p> Use the following templates to start crafting the message response from bot. </p>
                <FlatButton name="templateText" label="Text" primary={true} onClick={this.handleUseTemplate}/>
                <FlatButton name="templateTextActions" label="Text + Actions" secondary={true} onClick={this.handleUseTemplate}/>
                <FlatButton name="templateAttachment" label="Attachment" primary={true} onClick={this.handleUseTemplate}/>
                <FlatButton name="templateCard" label="Card" secondary={true} onClick={this.handleUseTemplate}/>
                <FlatButton name="templateLocation" label="Location" primary={true} onClick={this.handleUseTemplate}/>
                <FlatButton name="templateRaw" label="Raw" secondary={true} onClick={this.handleUseTemplate}/>
                </div>
                <JSONProperties 
                    properties={this.state.contentMessage}
                    returnProperties={this.setContentMessage}
                />
            </div>
        );
    }



}

WebhookSender.propTypes = {

    webhookConfig: PropTypes.shape({
      externalWebhookUrl: PropTypes.string,
      secretKey: PropTypes.string
    }).isRequired
    
}
  